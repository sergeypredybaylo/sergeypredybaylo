# Типы данных:


"""Task_1: Переменной var_int присвойте значение 10,var_float - значение 8.4,
var_str - "No"."""
var_int = 10
var_float = 8.4
var_str = "No"


"""Task_2: Измените значение, хранимое в переменной var_int,
увеличив его в 3.5 раза, результат свяжите с переменной big_int."""
var_int *= 3.5
big_int = var_int


"""Task_3: Измените значение, хранимое в переменной var_float,
уменьшив его на единицу, результат свяжите с той же переменной."""
var_float -= 1


"""Task_4: Разделите var_int на var_float, а затем big_int на var_float.
Результат данных выражений не привязывайте ни к каким переменным."""
print(var_int / var_float)
print(big_int / var_float)


"""Task_5: Измените значение переменной var_str на "NoNoYesYesYes".
При формировании нового значения используйте операции конкатенации (+)
и повторения строки (*)."""
a = "YES"
var_str = (var_str * 2) + (a * 3)


"""Task_6: Выведите значения всех переменных"""
print(var_int)
print(var_float)
print(var_str)


# Строки:


"""Task_1: Свяжите переменную с любой строкой, состоящей не менее чем из
8 символов. Извлеките из строки первый символ, затем последний, третий с
начала и третий с конца. Измерьте длину вашей строки."""
message = "Hello students!"
print(message[0])
print(message[-1])
print(message[3])
print(message[-3])
print(len(message))


"""Task_2: Присвойте произвольную строку длиной 10-15 символов переменной
и извлеките из нее следующие срезы:
- первые восемь символов
- четыре символа из центра строки
- символы с индексами кратными трем
- переверните строку"""
m = "Hello-students"
print(m[0:8])
q = len(m)
s = int(q / 2)
print(q)
print(m[(s - 2):(s + 2)])
print(m[::3])
print(m[::-1])


"""Task_3: Есть строка: “my name is name”. Напечатайте ее,
но вместо 2ого “name” вставьте ваше имя."""
my_name = 'my name is name'
my_name = my_name.replace('name', 'Kostya')
my_name = my_name.replace('Kostya', 'name', 1)
print(my_name)


"""Task_4: Есть строка: test_string = "Hello world!", необходимо
- напечатать на каком месте находится буква w
- кол-во букв l
- Проверить начинается ли строка с подстроки: “Hello”
- Проверить заканчивается ли строка подстрокой: “qwe”."""
test_string = "Hello world!"
print(test_string.find("w"))
print(test_string.count("l"))
print(test_string.startswith("Hello"))
print(test_string.endswith("qwe"))
