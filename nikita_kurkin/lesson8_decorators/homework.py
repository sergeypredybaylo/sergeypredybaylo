from decorators import simple_decorator, increase_sum, upper_letter, \
    func_name, change, new_some_decorator, ttimer, typed

import math

"""
Функция принимает 2 аргумента
Декоратор, который печатает имя функции и всего его аргументы
"""


@new_some_decorator('I want to smth')
def simple_function(a, b):
    """
    :param a: first some value
    :param b: second some value
    :return: tuple of values
    """
    return a, b


"""
Напишите декоратор, который печатает перед и
после вызова функции слова “Before” and “After”
"""


@simple_decorator
def what_the_sum(a, b):
    """
    :param a: first some integer
    :param b: second some integer
    :return: sum of 'a' and 'b'
    """
    return a + b


"""
Напишите функцию декоратор, которая добавляет 1 к заданному числу
"""


@increase_sum
def degree(a: int, b: int) -> int:
    """
    :param a: some integer
    :param b: digit which we used for raise to a degree
    :return: result of operation with degree
    """
    return a ** b


"""
Напишите функцию декоратор, которая
переводит полученный текст в верхний регистр
"""


@upper_letter
def some_string(s: str):
    """
    :param s: users' string
    :return: fixed string which include only upper letters
    """
    return s


"""
Напишите декоратор func_name,
который выводит на экран имя функции (func.__name__)
"""


@func_name
def some_string1():
    """
    :return: sum of random digits
    """
    return 5 + 5


"""
Напишите декоратор change, который делает так, что задекорированная
функция принимает все свои не именованные аргументы в порядке, обратном
тому, в котором их передали
"""


@change
def some_random_function(*args):
    """
    :param args: random users' args
    :return: reversed users' args
    """
    return args


"""
Напишите декоратор, который вычисляет время работы декорируемой функции (timer)
"""


@ttimer
def new_list_compreh(n):
    """
    :param n: length of list
    :return: list of numbers which were raise to a degree
    """
    lst = [i ** 6 for i in range(n)]
    return lst


"""
Напишите функцию, которая вычисляет значение числа Фибоначчи для
заданного количество элементов последовательности и возвращает
его и оберните ее декоратором timer и func_name
"""


@ttimer
@func_name
def fib(n):
    """
    :param n: length of fibbonachchi list
    :return: list with results of operated fib function
    """
    fib_list = []
    a, b = 0, 1
    while a < n:
        fib_list.append(a)
        a, b = b, a + b
    return fib_list


"""
Напишите функцию, которая вычисляет сложное математическое
выражение и оберните ее декоратором из пункта 1, 2
"""


@simple_decorator
@increase_sum
def some_mathematics_example(x, y):
    """
    :param x: factorial of digit
    :param y: some multiplier
    :return: result of math operation with sin and factorial
    """
    return math.sin(math.pi / 2) * y ** math.factorial(x)


"""
Напишите декоратор, который проверял бы тип параметров функции,
конвертировал их если надо и складывал:

@typed(type=’str’)
def add_two_symbols(a, b):
    return a + b

add_two_symbols("3", 5) -> "35"
add_two_symbols(5, 5) -> "55"
add_two_symbols('a', 'b') -> 'ab’

@typed(type=’int’)
def add_three_symbols(a, b, с):
    return a + b + с

add_three_symbols(5, 6, 7) -> 18
add_three_symbols("3", 5, 0) -> 8
add_three_symbols(0.1, 0.2, 0.4) -> 0.7000000000000001

"""


@typed(type='str')
def add_two_symbols(a, b):
    """
    :param a: some value 1
    :param b: some value 2
    :return: result of concatenation 'a' and 'b'
    """
    return a + b


@typed(type='int')
def add_three_symbols(a, b, с):
    """
    :param a: some value 1
    :param b: some value 2
    :param с: some value 3
    :return: result of sum 'a', 'b', 'c'
    """
    return a + b + с
