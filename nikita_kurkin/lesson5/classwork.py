"""
IF
1. Если значение некой переменной больше 0, выводилось бы специальное
сообщение. Один раз выполните программу при значении переменной больше 0,
второй раз — меньше 0.
"""
a = 10
b = -2
if a > 0:
    print('Digit "a" more than 0')

if b < 0:
    print('Digit "b" is less than 0')

'''
2. Усовершенствуйте предыдущий код с помощью ветки else так, чтобы в
зависимости от значения переменной, выводилась либо 1, либо -1.
'''
a = 5
# a = -2

if a > 0:
    print('1')
else:
    print('-1')

'''
3. Самостоятельно придумайте программу, в которой бы
использовалась инструкция if(желательно с веткой elif).
Вложенный код должен содержать не менее трех выражений.
'''
a = -3

if a > 0:
    print(a)
    print("'a' is more than 0")
elif a == 0:
    print("'a' is equal 0")
    a += 1
else:
    print("'a' is less than 0")
    a -= 1
    print(a)

'''
ELIF
1. Напишите программу по следующему описанию:
a. двум переменным присваиваются числовые значения;
b. если значение первой переменной больше второй, то найти разницу значений
переменных (вычесть из первой вторую), результат связать с третьей переменной;
c. если первая переменная имеет меньшее значение, чем вторая, то третью
переменную связать с результатом суммы значений двух первых переменных;
d. во всех остальных случаях, присвоить третьей переменной значение первой
переменной;
e. вывести значение третьей переменной на экран.
'''
digit_1 = 5
digit_2 = 11

if digit_1 > digit_2:
    result = digit_1 - digit_2
elif digit_1 < digit_2:
    result = digit_1 + digit_2
else:
    result = digit_1
print(result)

'''
2. Придумайте программу, в которой бы использовалась инструкция if-elif-else.
Количество ветвей должно быть как минимум четыре.
'''

my_cash = 12_000_000
favorite_country = 'Japan'

if 10_000_000 > my_cash < 11_500_000 and 'Germany' in favorite_country:
    car_name = 'Mercedes'
elif 11_500_000 > my_cash < 14_000_000 and \
        ('Spain' or 'UK' in favorite_country):
    car_name = 'Bentley'
elif 12_000_000 >= my_cash < 14_000_000 and 'Japan' in favorite_country:
    car_name = 'Honda'
elif 12_000_000 >= my_cash < 12_100_000 and 'Germany' in favorite_country:
    car_name = 'Audi'
else:
    car_name = 'zaporojec'
print(car_name)

'''
FOR
1. Создайте список, состоящий из четырех строк. Затем, с помощью цикла for,
выведите строки поочередно на экран.
2. Измените предыдущую программу так, чтобы в конце каждой буквы строки
добавлялось тире. (Подсказка: цикл for может быть вложен в другой цикл.)
'''
some_list = ['abc', 'def', 'ghi', 'jkl']
for i in some_list:
    print(i)

for i in some_list:
    for j in i:
        if j == i[-1]:
            j = j + '-'
            print(j)

'''
3. Создайте список, содержащий элементы целочисленного типа, затем с помощью
цикла перебора измените тип данных элементов на числа с плавающей точкой.
(Подсказка: используйте встроенную функцию float().)
'''
some_digit_list = [10, 4, 2, 5, 6, 23, 14]
for i in some_digit_list:
    i = float(i)
    print(type(i))

'''
*4. Есть два списка с одинаковым количеством элементов, в каждом из них этих
списков есть элемент “hello”. В первом списке индекс этого элемента 1,
во втором 7.
Одинаковых элементов в обоих списках может быть несколько. Напишите программу
которая распечатает индекс слова “hello” в первом списке и
условный номер списка, индекс слова во втором списки и
условный номер списка со словом Совпадают. Например, программа
выведет на экран:
“Совпадают 1-й элемент из первого списка и 7-й элемент из второго
списка”
'''
a = ['hi', 'hello', 'hau hai', 'something', 'else',
     'between', 'qq', 'chao a tutti']
b = ['hey', 'qq', 'privet', 'poka', 'poki poiki', 'hey', 'hey', 'hello']

for i in a:
    for j in b:
        if i == j:
            print(f'Совпадают {a.index(i)}-й элемент из первого списка'
                  f' и {b.index(j)}-й элемент из второго списка')

'''
WHILE
1. Напишите скрипт на языке программирования Python, выводящий ряд чисел
Фибоначчи (числовая последоватьность в которой числа начинаются с 1 и 1 или
же и 0 и 1, пример: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, ...).
Запустите его на выполнение. Затем измените код так, чтобы выводился
ряд чисел Фибоначчи, начиная с пятого члена ряда и заканчивая двадцатым.
'''

n = 1000
a, b = 0, 1
digits = []
while a < n:
    digits.append(a)
    a, b = b, a + b
print(digits[5:20])

'''
2. Напишите цикл, выводящий ряд четных чисел от 0 до 20. Затем, каждое третье
число в ряде от -1 до -21.
'''
some_digits = [i for i in range(21)]
for digit in some_digits:
    if digit % 2 == 0:
        print(digit)
print()

some_digits_1 = [i for i in range(-21, 0)]
for digit in some_digits_1:
    if digit % 3 == 0:
        print(digit)
print()

'''
3. Самостоятельно придумайте программу на Python, в которой
бы использовался цикл while.
'''
a = 10
while a > 0:
    print(a)
    a -= 2

'''
input()
1. Напишите программу, которая запрашивает у пользователя:
- его имя (например, "What is your name?")
- возраст ("How old are you?")
- место жительства ("Where are you live?")
После этого выводила бы три строки:
"This is имя"
"It is возраст"
"(S)he lives in место_жительства"
'''
name = input('What\'s your name? ')
age = int(input('How old are you? '))
location = input('Where are you live? ')

print(f'This is {name}')
print(f'It is {age}')
print(f'(S)he lives in {location}')


'''
2. Напишите программу, которая предлагала бы пользователю решить пример 4 *
100 - 54. Потом выводила бы на экран правильный ответ и ответ пользователя.
Подумайте, нужно ли здесь преобразовывать строку в число.
'''
result = 4 * 100 - 54
own_result = input('Solve the equation "4 * 100 - 54": ')
if result == int(own_result):
    print('Congrats! It\'s true!')
else:
    print('Something wrong! Try to resolve this!')


'''
3. Запросите у пользователя четыре числа. Отдельно сложите первые два и
отдельно вторые два. Разделите первую сумму на вторую. Выведите результат
на экран так, чтобы ответ содержал две цифры после запятой.
'''

a = input('Enter the first digit: ')
b = input('Enter the second digit: ')
c = input('Enter the third digit: ')
d = input('Enter the fourth digit: ')
total_result = round((int(a + b) / int(c + d)), 2)
print(total_result)


'''
1. Из заданной строки получить нужную строку:
"String" => "SSttrriinngg"
"Hello World" => "HHeelllloo WWoorrlldd"
"1234!_ " => "11223344!!__ "
'''
str_ = list("String")

for i in range(len(str_)):
    str_[i] = str_[i] + str_[i]
print(''.join(str_))

'''
Если х в квадрате больше 1000 - пишем "Hot" иначе - "Nope"
'50' == "Hot"
4 == "Nope"
"12" == "Nope"
60 == "Hot"
'''
x = '12'
if type(x) != str and x ** 2 > 1000:
    # очень тянутся руки написать блок try except
    print('Hot')
else:
    print('Nope')

'''
Подсчет букв
Дано строка и буква => "fizbbbuz","b", нужно подсчитать
сколько раз буква b встречается в заданной строке 3

"Hello there", "e" == 3
"Hello there", "t" == 1
"Hello there", "h" == 2
"Hello there", "L" == 2
"Hello there", " " == 1
'''
string_ = "fizbbbuz"
print(string_.count('b'))

'''
Напишите программу, которая по данному числу n от 1 до
n выводит на экран n флагов. Изображение одного флага имеет
размер 4×4 символов. Внутри каждого флага должен быть записан
его номер — число от 1 до n.

'''
n = 5
for i in range(n + 1):
    print('+_')
    print(f'|{i} /')
    print('|_ \\')
    print('|')

'''
Напишите программу. Есть 2 переменные salary и bonus. Salary - integer,
bonus - boolean. Если bonus - true, salary должна быть умножена на 10.
Если false - нет

10000, True == '$100000'
25000, True == '$250000'
10000, False == '$10000'
60000, False == '$60000'
2, True == '$20'
78, False == '$78'
67890, True == '$678900'
'''
salary = 10000
bonus = False
if bonus:
    salary_new = salary * 10
    print(f'{salary}, {bonus} == \'${salary_new}\'')
else:
    print(f'{salary}, {bonus} == \'${salary}\'')

'''
Суммирование. Необходимо подсчитать сумму всех чисел до заданного:
дано число 2, результат будет -> 3(1+2)
дано число 8, результат будет -> 36(1 + 2 + 3 + 4 + 5 + 6 + 7 + 8)

1 == 1
8 == 36
22 == 253
100 == 5050
'''
n = 8
sum_ = 0
for i in range(n + 1):
    sum_ += i
print(sum_)
