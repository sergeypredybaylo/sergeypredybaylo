"""
Hero
В игре есть солдаты и герои. У каждого есть уникальный номер,
и свойство, в котором хранится принадлежность команде.
У солдат есть метод "move_to_hero", который в качестве аргумента
принимает объект типа "Hero". У героев есть метод увеличения
собственного уровня level_up.
В основной ветке программы создается по одному герою для каждой
команды. В цикле генерируются объекты-солдаты. Их принадлежность
команде определяется случайно. Солдаты разных команд добавляются
в разные списки.
Измеряется длина списков солдат противоборствующих команд и
выводится на экран. У героя, принадлежащего команде с более
длинным списком, поднимается уровень.
Отправьте одного из солдат первого героя следовать за ним.
Выведите на экран идентификационные номера этих двух юнитов.
"""
import random


class Hero:
    _unique_hero_id = 0
    level = 1

    def __init__(self, team):
        self._unique_hero_id += 1
        self.team = team

    def level_up(self):
        self.level += 1


class Soldiers:
    _unique_soldier_id = 1

    def __init__(self, team):
        self._id = self._unique_soldier_id
        self.team = team
        Soldiers.increase_id_of_soldiers()

    def move_to_hero(self, Hero):
        return f'I am from {Hero} team! Go fight! I am a {Hero._id}-th!'

    def __str__(self):
        return f'{self.team}'

    @classmethod
    def increase_id_of_soldiers(cls):
        cls._unique_soldier_id += 1


side = ['Red', 'Blue']

hero1 = Hero('Red')
soldier_1 = []
hero2 = Hero('Blue')
soldier_2 = []

for i in range(20):
    a = random.choice(side)
    if a == 'Red':
        soldier_1.append(Soldiers('Red'))
    else:
        soldier_2.append(Soldiers('Blue'))

red_sold_1 = soldier_1[0]
blue_sold_1 = soldier_2[0]
red_sold_1.move_to_hero(red_sold_1)
blue_sold_1.move_to_hero(blue_sold_1)

print(len(soldier_1))
print(len(soldier_2))

if len(soldier_1) > len(soldier_2):
    hero1.level_up()
    print('Hero1 is level up')
elif len(soldier_1) < len(soldier_2):
    hero2.level_up()
    print('Hero2 is level up')
elif len(soldier_1) == len(soldier_2):
    print('Both of heroes have the same count of soldiers')
else:
    print('Something wrong! Try again it!')
