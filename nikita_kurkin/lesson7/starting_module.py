counter = 0
while counter < 3:
    counter += 1
    what_user_want = input("Enter the number that you want: \n"
                           "1) bank and deposits \n"
                           "2) Cesar encode and decode \n->")
    if what_user_want == '1':
        from hw_bank import hw_bank
        print(hw_bank.hw_bank(10000, 3))
    elif what_user_want == '2':
        from hw_own_decoder import hw_own_decoder
        print(hw_own_decoder.encode_or_decode('hello world!', 3))
        print(hw_own_decoder.encode_or_decode('this is a test string', 5))
        print(hw_own_decoder.encode_or_decode('khoor zruog!', -3))
        print(hw_own_decoder.encode_or_decode('ymnx nx f yjxy xywnsl', -5))
    elif what_user_want == 'q':
        print('Program will be stopped!')
        break
    else:
        print('Incorrect value!')
