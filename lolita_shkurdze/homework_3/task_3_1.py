import random
import math

print('Task 1')
# Заменить символ “#” на символ “/” в строке 'www.my_site.com#about'
s = 'www.my_site.com#about'
print(s.replace('#', '/'))


print('Task 2')
# В строке “Ivanou Ivan” поменяйте местами слова => "Ivan Ivanou"
name = "Ivan"
surname = "Ivanou"
print(f"{name} {surname}")


print('Task 3')
# Напишите программу которая удаляет пробел в начале строки
s = '     Hello!'
print(s.lstrip())

print('Task 4')
# Напишите программу которая удаляет пробел в конце строки
s = 'Hello!        '
print(s.rstrip())


print('Task 5')
# a = 10, b = 23, поменять значения местами, чтобы в переменную
# “a” = “23”, “b” = “-10”
a = 10
b = 23
a, b = b, -a
print(a, b)


print('Task 6')
# значение переменной “a” увеличить в 3 раза, а значение “b”
# уменьшить на 3
a = 20
b = 103
print(a * 3, 'and', b - 3)


print('Task 7')
# преобразовать значение “a” из целочисленного в число с плавающей
# точкой (float), а значение в переменной “b” в строку
print(float(a))
print(str(b))


print('Task 8')
# Разделить значение в переменной “a” на 11 и вывести результат с
# точностью 3 знака после запятой
a = 111
print(round(a / 11, 3))


print('Task 9')
# Преобразовать значение переменной “b” в число с плавающей точкой и
# записать в переменную “c”. Возвести полученное число в 3-ю степень.
c = float(b)
print(pow(c, 3))


print('Task 10')
# Получить случайное число, кратное 3-м
print('Random number:', random.randrange(3, 33, 3))


print('Task 11')
# Получить квадратный корень из 100 и возвести в 4 степень

print('Корень числа 100 в 4 степени =:', (math.sqrt(100) ** 4))


print('Task 12')
# Строку “Hi guys” вывести 3 раза и в конце добавить “Today”
# (ex.“Hi guysHi guysHi guysToday”)
str1 = "Hi guys"
str2 = "Today"
str3 = str1 * 3 + str2
print(str3)


print('Task 13')
# Получить длину строки из предыдущего задания
print('Длина строки:', len(str3), 'знаков')


print('Task 14')
# Взять предыдущую строку и вывести слово “Today” в прямом и обратном
# порядке
b1 = 'Hi guysHi guysHi guysToday'
a1 = b1[-5:]
print('Прямой порядок:', b1[-5:])
print('Обратный порядок:', a1[::-1])


print('Task 15')
# “Hi guysHi guysHi guysToday” вывести каждую 2-ю букву
# в прямом и обратном порядке
b1 = 'Hi guysHi guysHi guysToday'

print('Прямой порядок:', b1[::2])
print('Обратный порядок:', b1[::-2])

print('Task 16')
# не решено

# Используя форматирования подставить результаты из задания
# 10 и 11 в следующую строку:
# Task 10: <в прямом>, <в обратном>
# Task 11: <в прямом>, <в обратном>”

print('Task 17')
# Есть строка: “my name is name”. Напечатайте ее, но вместо 2ого “name”
# вставьте ваше имя.
a = 'Lola'
print("My name is {name}".format(name=a))


print('Task 18')
# Полученную строку в задании 12 вывести:
# а) Каждое слово с большой буквы
# б) все слова в нижнем регистре
# в) все слова в верхнем регистре

txt = 'Hi guysHi guysHi guysToday'
print("Title\t\t", txt.title())
print("Lower\t\t", txt.lower())
print("Upper\t\t", txt.upper())


print('Task 19')
# Посчитать сколько раз слово “Task” встречается в строке из задания 12
txt = 'Hi guysHi guysHi guysToday'
print('Слово "Task" встречается', txt.count('Task'), 'раз')
