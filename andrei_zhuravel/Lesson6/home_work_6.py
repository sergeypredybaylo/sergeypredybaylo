# Task 1.Validate
# Ваша задача написать программу, принимающее число - номер кредитной
# карты(число может быть четным или не четным). И проверяющей может ли
# такая карта существовать. Предусмотреть защиту от ввода букв,
# пустой строки и т.д.
# Примечания
# Алгоритм Луна
# Примеры
# validate(4561261212345464) #=> False
# validate(4561261212345467) #=> True

from functools import reduce
code = input("Please, enter card number : ").replace(" ", "")


def luhn(code: str):
    """
    Function that checks if such a card can exist
    """
    while len(code) == 0:
        code = input("Please, enter card number : ").replace(" ", "")
    while code.isdigit() is False:
        code = input("Card number is not correct, please,"
                     " enter correct card number: ").replace(" ", "")
    lookup = (0, 2, 4, 6, 8, 1, 3, 5, 7, 9)
    code = reduce(str.__add__, filter(str.isdigit, code))
    evens = sum(int(i) for i in code[-1::-2])
    odds = sum(lookup[int(i)] for i in code[-2::-2])
    return ((evens + odds) % 10 == 0)


print("Task 1. Validation: ", luhn(code))
print(luhn.__doc__)


# Task 2. Подсчет количества букв
# На вход подается строка, например, "cccbba" результат работы программы
# - строка “c3b2a"
# Примеры для проверки работоспособности:
# "cccbba" == "c3b2a"
# "abeehhhhhccced" == "abe2h5c3ed"
# "aaabbceedd" == "a3b2ce2d2"
# "abcde" == "abcde"
# "aaabbdefffff" == "a3b2def5"
enter_string = input("Task 2. Please,enter your string: ")


def letter_counter(enter_string: str):
    """
    Function that counts repeated letters in a row in the entered string
    """
    x = []
    for i in enter_string:
        y = i + str(enter_string.count(i))
        if y not in x:
            x.append(y)
    x = ''.join(x)
    return x


print("Task 2.", letter_counter(enter_string))
print(letter_counter.__doc__)


# Task 3. Простейший калькулятор v0.1
# Реализуйте программу, которая спрашивала у пользователя, какую операцию
# он хочет произвести над числами, а затем запрашивает два числа и
# выводит результат
# Проверка деления на 0.
# Пример
# Выберите операцию:
#     1. Сложение
#     2. Вычитание
#     3. Умножение
#     4. Деление
# Введите номер пункта меню:
# >>> 4
# Введите первое число:
# >>> 10
# Введите второе число:
# >>> 3
# Частное: 3, Остаток: 3
operation = int(input("Task 3. Выберите перацию:\n 1. Сложение \n"
                      " 2. Вычитание \n 3. Умножение \n 4. Деление \n "
                      "Введите выбрыную операцию: "))


def calc(operation: int):
    """
    Сalculator algorithm based on operating conditions
    """
    a = int(input("Введите первое число: "))
    b = int(input("Ведите второе число: "))
    if operation == 1:
        return a + b
    elif operation == 2:
        return a - b
    elif operation == 3:
        return a * b
    elif operation == 4:
        if b == 0:
            return 0
        return a / b


print(round(calc(operation), 1))
print(calc.__doc__)


# Task 4. Написать функцию с изменяемым числом входных параметров
# При объявлении функции предусмотреть один позиционный и один именованный
# аргумент, который по умолчанию равен None (в примере это аргумент с
# именем name). Также предусмотреть возможность передачи нескольких
# именованных и позиционных аргументов
# Функция должна возвращать следующее
# result = function(1, 2, 3, name=’test’, surname=’test2’,
# some=’something’)
# Print(result)
# 🡪 {“mandatory_position_argument”: 1, “additional_position_arguments”:
# (2, 3), “mandatory_named_argument”: {“name”: “test2”},
# “additional_named_arguments”: {“surname”: “test2”, “some”: “something”}}
def my_function(x, *args, name=None, **kwargs):
    """
    Function describing input parameters
    """
    return {"mandatory_position_argument": x,
            "additional_position_arguments": args,
            "mandatory_named_argument": {"name": name},
            "additional_named_arguments": {**kwargs}
            }


result = my_function(1, 2, 3, name="test", surname="test2", some="something")
print("Task 4. ", result)
print(my_function.__doc__)


# Task 5. Работа с областями видимости
# На уровне модуля создать список из 3-х элементов
# Написать функцию, которая принимает на вход этот список и
# добавляет в него элементы. Функция должна вернуть измененный список.
# При этом исходный список не должен измениться. Пример c функцией
# которая добавляет в список символ “a”:
# My_list = [1, 2, 3]
# Changed_list = change_list(My_list)
# Print(My_list)
# 🡪 [1, 2, 3]
# Print(Changed_list)
# 🡪 [1, 2, 3, “a”]
my_list = [1, 2, 3]


def add_element(my_list: list):
    """
    Adding element to list
    """
    changed_list = my_list + [input("Enter adding element: ")]
    return changed_list


print("Task 5. ", add_element(my_list))
print(add_element.__doc__)


# Task 6. Функция проверяющая тип данных
# Написать функцию которая принимает на фход список из чисел, строк и таплов
# Функция должна вернуть сколько в списке элементов приведенных данных
# print(my_function([1, 2, “a”, (1, 2), “b”])
# 🡪 {“int”: 2, “str”: 2, “tuple”: 1}
my_new_function = [1, 2, "a", (1, 2), "b"]


def el_type_counter(my_new_function: list):
    """
    Determines the data type and counts the same type
    """
    x = 0
    y = 0
    z = 0
    for i in range(len(my_new_function)):
        if isinstance(my_new_function[i], int):
            x += 1
        if isinstance(my_new_function[i], str):
            y += 1
        if isinstance(my_new_function[i], tuple):
            z += 1
    data_type = {'int': x, 'str': y, 'tuple': z}
    return data_type


print("Task 6. : ", el_type_counter(my_new_function))
print(el_type_counter.__doc__)


# Task 7. Написать пример чтобы hash от объекта 1 и 2 были одинаковые,
# а id разные.
a = 1
b = 1.0
print("Task 7. : hash - ", hash(a) == hash(b))
print("Task 7. : id - ", id(a) == id(b))


# Task 8. написать функцию, которая проверяет есть ли в списке объект,
# которые можно вызвать

def callable_or_not(my_list: list):
    """
    This function checks is callable object exists in the list or not.
    """
    return [callable(call) for call in my_list]


my_list = [int, "yes", -2, 1.3, True, callable_or_not]
print("Task 8. ", callable_or_not(my_list))
print(callable_or_not.__doc__)
