# task2.1. Свяжите переменную с любой строкой,
# состоящей не менее чем из 8 символов.
# Извлеките из строки первый символ, затем последний,
# третий с начала и третий с конца.
# Измерьте длину вашей строки.
str1 = 'qwerty type are fellas'
print(str1[0])
print(str1[-1])
print(str1[2])
print(str1[-3])
print(len(str1))
print(

)


# task2.2. Присвойте произвольную строку длиной 10-15 символов переменной
# и извлеките из нее следующие срезы:
# 1. первые восемь символов
# 2. четыре символа из центра строки
# 3. символы с индексами кратными трем
# 4. переверните строку
str2 = 'as2df3gh4jk5yrt'
print(str2[0:8])
sq = len(str2)
sm = int(sq / 2)
print(sq)
print(str2[(sm - 2):(sm + 2)])
print(str2[::3])
print(str2[::-1])


# task2.3. Есть строка: “my name is name”.
# Напечатайте ее, но вместо 2ого “name” вставьте ваше имя.
str3 = 'Andrei'
print('my name is {name}'.format(name=str3))


# task2.4. Есть строка: test_string = "Hello world!",
# необходимо напечатать на каком месте находится буква w кол-во букв l
# Проверить начинается ли строка с подстроки: “Hello”
# Проверить заканчивается ли строка подстрокой: “qwe”
test_string = "Hello world!"
print(test_string.find('w'))
print(test_string.count('l'))
print(test_string.startswith('Hello'))
print(test_string.endswith('qwe'))
