from home_work8 import decorator, inc_decorator, decorator2
from home_work8 import decorator_fun_name, change, timer
from home_work8 import decorator_converter


# Task 1


@decorator
def empty_function():
    return None


empty_function()

# Task 2


@inc_decorator
def inc_function(x):
    print('Called inc_function', x)


intVal = 2
resVal = inc_function(intVal)

# Task 3


@decorator2
def text_upper(y):
    print('Called inc_function', y)


text = "Dima is Grud"
print(text)
text_upper(text)
print(text)


# Task 4

@decorator_fun_name
def print_function_name():
    print('I wrapped to print the name')


def function_to_print():
    return


print_function_name(function_to_print)

# Task 5


@change
def fifth_task(*args):
    """
    This function takes one argument and returns it.
    """
    return args


result = fifth_task(2, 3, 4)
print(result)

# Task 6


@timer
def func_to_measure():
    """
        This function takes one argument and returns it. There is a rubbish
        inside function to make it more difficult and check the speed of a
        processor.
        """
    list_1 = [i**5 for i in range(100)]
    return list_1


result = func_to_measure()
print(result)

# Task 7
"""7. Напишите функцию, которая вычисляет значение числа
 Фибоначчи для заданного количество элементов последовательности
  и возвращает его и оберните ее декоратором timer и func_name
"""


@timer
@decorator_fun_name
def fib_func(n):
    a = 0
    b = 1
    for i in range(n):
        a, b = b, a + b
    return a

# Task 8


"""8. Напишите функцию, которая вычисляет сложное
математическое выражение и оберните ее
декоратором из пункта 1, 2
"""


@decorator  # первый декоратор
@inc_decorator  # второй декоратор
def math_fun(x):
    y = x ** 3
    print('math_fun', y)


math_fun(3)


# second way
@decorator  # первый декоратор
def math_fun(x):
    y = x ** 3
    print('math_fun', y)


math_fun(3)


@inc_decorator  # второй декоратор
def math_fun(x):
    # функция делает возвидение в степень
    y = x ** 3
    print('math_fun', y)


math_fun(3)


# Task 9


@decorator_converter(type_to_conver_to='sting')
def add_two_symbols(a, b):
    return a + b


result = add_two_symbols(1, 2)
print(result)

print("{} -> {}".format("add_two_symbols(\"3\", 5)",
                        add_two_symbols("3", 5)))
print("{} -> {}".format("add_two_symbols(5, 5)",
                        add_two_symbols(5, 5)))
print("{} -> {}".format("add_two_symbols('a', 'b')",
                        add_two_symbols('a', 'b')))


@decorator_converter(type_to_conver_to='digit')
def add_three_symbols(a, b, c):
    return a + b + c


add_three_symbols(5, 6, 7)

print("{} -> {}".format("add_two_symbols(5, 6, 7)",
                        add_three_symbols(5, 6, 7)))
print("{} -> {}".format("add_two_symbols(\"3\", 5, 0)",
                        add_three_symbols("3", 5, 0)))
print("{} -> {}".format("add_two_symbols(0.1, 0.2, 0.4)",
                        add_three_symbols(0.1, 0.2, 0.4)))
