from selenium.webdriver.common.by import By


class FictionPageLocators:
    FICTION_LINK = (By.XPATH,
                    ' //*[@id="default"]/div[2]/div/div'
                    '/aside/div[2]/ul/li[2]/ul/li[1]/a')
    HEADER_FICTION_PAGE = (By.XPATH, '//h1[text()="Fiction"]')
