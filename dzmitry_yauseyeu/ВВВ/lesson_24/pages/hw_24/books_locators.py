from selenium.webdriver.common.by import By


class BookPageLocators:
    BOOKS_LINK = (By.XPATH, '// a[text() = "Books"]')
    first_book = (By.XPATH, "//*[@title=\"The shellcoder's handbook\"]")
    selected_book_idicator = (By.XPATH, '//* [@class="price_color"]')
    Seeker_OF_BOOKS = (By.XPATH, '//a[@title]')
    TITLE = "The shellcoder's handbook"
