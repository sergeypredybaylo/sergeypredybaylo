"""создайте класс работник
У класса есть конструктор, в который надо передать имя и ЗП.
У класса есть 2 метода: получение имени сотрудника
 и получение количества созданных работников
При создании экземпляра класса должен вызываться метод,
 который отвечает за увеличение количества работников на 1."""


class Worker:

    count_of_people = 0

    def __init__(self, name, salary):
        self.name = name
        self.salary = salary
        Worker.count_of_people += 1

    def get_worker_name(self):
        return self.name

    @classmethod
    def get_worker_count(cls):
        return cls.count_of_people


name_of_peop1 = Worker("Dima", 100)
print(Worker.count_of_people)


"""
Путешествие
Вы идете в путешествие, надо подсчитать
 сколько у денег у каждого студента.
Класс студен описан следующим образом:
class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money
Необходимо понять у кого больше всего денег
 и вернуть его имя. Если у студентов
денег поровну вернуть: “all”.
(P.S. метод подсчета не должен быть частью класса)
assert most_money([ivan, oleg, sergei]) == "all"
assert most_money([ivan, oleg]) == "ivan"
assert most_money([oleg]) == "Oleg"
"""


class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money


def most_money(arg):
    student_money = [student.money for student in arg]
    max_money = max(student_money)
    if student_money.count(max_money) == len(student_money):
        return "all"
    for student in arg:
        if student.money == max_money:
            return student.name


ivan = Student("ivan", 220)
oleg = Student("oleg", 280)
sergei = Student("sergei", 220)

assert most_money([ivan, oleg, sergei]) == "oleg"
print(most_money([ivan, oleg, sergei]))
