# Создайте класс book с именем книги,
# автором, кол-м страниц, ISBN, флагом,
# зарезервирована ли книги или нет.
# Создайте класс пользователь который может
# брать книгу, возвращать, бронировать.
# Если другой пользователь хочет взять
# зарезервированную книгу(или которую
# уже кто-то читает - надо ему про это
# сказать).
class Book:

    def __init__(self, name, author, pages, isbn):
        self.name = name
        self.author = author
        self.pages = pages
        self.isbn = isbn
        self.available = True


book1 = Book(
    'For whom the bell tolls',
    'Hemingway',
    576, 9781476787817
)
book2 = Book(
    'Misery',
    'King',
    320, 91501156748
)


class User:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name
        self.books_taken = []
        self.books_reserved = []

    def borrow(self, book):
        if book in self.books_reserved or book.available:
            print('Success!')
            self.books_taken.append(book)
            if book in self.books_reserved:
                self.books_reserved.remove(book)
            book.available = False
        else:
            print('Sorry! This book is temporarily unavailable.')

    def reserve(self, book):
        if not book.available and book not in self.books_reserved:
            print('Sorry! This book is temporarily unavailable.')
        elif book in self.books_reserved:
            print('You have already reserved this book!')
        else:
            print('Success!',
                  'The book has been added'
                  ' to your list of reserved books.',
                  sep='\n')
            self.books_reserved.append(book)
            book.available = False

    def give_back(self, book):
        if book in self.books_taken:
            print('Success!')
            self.books_taken.remove(book)
            book.available = True
        else:
            print('Nothing to return!')

    def cancel_reservation(self, book):
        if book in self.books_reserved:
            print('Your reservation has been'
                  ' successfully canceled.')
            book.available = True
        else:
            print('Sorry! This book is not in'
                  ' your reservation list.')


user1 = User('Josh', 'Parrott')
user2 = User('Lily', 'Griffin')
user3 = User('Kate', 'Walker')

user1.reserve(book1)
user2.reserve(book1)
user2.cancel_reservation(book1)
user1.cancel_reservation(book1)
user3.reserve(book1)
user2.borrow(book1)
user3.borrow(book1)
user2.give_back(book1)
user3.give_back(book2)
user3.give_back(book1)


class Investment:

    def __init__(self, n, r):
        self.principal = n
        self.total_years = r


inv1 = Investment(1000, 5)


class Bank:
    an_int = 0.1   # annual interest
    c = 12         # compounding periods per year

    @classmethod
    def deposit(cls, investment):
        final_amount = investment.principal * (
            pow((1 + Bank.an_int / Bank.c), Bank.c * investment.total_years)
        )
        currency = "${:,.2f}".format(final_amount)
        return currency


print(Bank.deposit(inv1))
