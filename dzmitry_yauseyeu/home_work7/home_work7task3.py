import random

"""
Общий модуль
Создать модуль, который будет предоставлять возможность выбрать одну из двух
предыдущих программ и запустить ее, также он должен позволить по окончанию
выполнения программы выбрать запуск другой!
Если пользователь совершит ошибку при выборе программы
Первые три раза программа должна сообщить ему об
ошибке, а на четвертый раз
запустить одну из программ, выбранных случайно
"""


def module():
    """the function launches the selected module,
    there are 4 attempts to launch otherwise a
     random module is launched,
    next restart or exit.
    """
    count = 0
    while count < 3:
        count += 1
        var_mod = int(input(" 1 - Шифр цезаря,"
                            " 2 - Банковский вклад, 3 - выход: "))
        if count >= 3:
            var_mod = random.randint(1, 2)
        elif var_mod == 1:
            import home_work7
            print(home_work7.coder())
        elif var_mod == 2:
            import home_work7
            print(home_work7.bank())
        elif var_mod == 3:
            exit()
        else:
            print("ошибка!")
    return


module()
while True:
    module()
