import random
"""Вы идете в путешествие,
надо подсчитать сколько у денег у
каждого студента. Класс студен описан
 следующим образом:"""


class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money
    # Property is used to convert USD to RUB

    @property
    def byn(self):
        return self.money * 2.5

    @byn.setter
    def byn(self, value_byn):
        self.money = value_byn / 2.5


def most_money(arg):
    # The maximum amount of money is determined and
    # displays the name of who has more money
    student_money = [student.byn for student in arg]
    max_money = max(student_money)
    if student_money.count(max_money) == len(student_money):
        return "all"
    for student in arg:
        if student.byn == max_money:
            return student.name


ivan = Student("ivan", 220)
oleg = Student("oleg", 280)
sergei = Student("sergei", 290)

# assert most_money([ivan, oleg, sergei]) == "oleg"
print(most_money([ivan, oleg, sergei]))

"""В игре есть солдаты и герои. У каждого есть уникальный номер,
и свойство, в котором хранится принадлежность команде.
У солдат есть метод "move_to_hero", который в качестве
аргумента принимает объект типа "Hero". У героев есть метод
увеличения собственного уровня level_up.
В основной ветке программы создается по одному герою для
каждой команды. В цикле генерируются объекты-солдаты.
Их принадлежность команде определяется случайно.
Солдаты разных команд добавляются в разные списки.
Измеряется длина списков солдат противоборствующих
команд и выводится на экран. У героя, принадлежащего команде
с более длинным списком, поднимается уровень.
Отправьте одного из солдат первого героя следовать
 за ним. Выведите на экран идентификационные номера этих двух юнитов.
"""

unitCounter = 0


class Unit:
    # class of units with relevant attributes
    soldierPropertyID = 1
    heroPropertyID = 2

    unq_number = None
    property_entity = None


def __init__(self, property_entity):
    # initialisation attributes and established
    # global item unitCounter
    global unitCounter
    self.unq_number = unitCounter
    self.property_entity = property_entity
    unitCounter += 1


class Soldier(Unit):
    # class soldier is inherited from unit
    # method for creating soldier with unq id
    # move_to_hero method works for Move Soldier To Hero
    def __init__(self):
        super().__init__(self.soldierPropertyID)
        print("create Soldier with ID {}", self.unq_number)

    def move_to_hero(self, hero):
        print("Move Soldier {} To Hero {}"
              .format(self.unq_number, hero.unq_number))
        return


class Hero(Unit):

    level = 0

    # initialisation attributes and established item level
    # class Hero is inherited from unit
    # method for creating Hero with unq id
    # and level_up for improve level of Hero
    def __init__(self):
        super().__init__(self.heroPropertyID)
        print("create Hero with ID {}".format(self.unq_number))

    def level_up(self):
        self.level = self.level + 1


teamsCount = 5
soldierCount = teamsCount * 3

teamCounter = 0


class Team:
    commandSoldierList = None
    heroUnit = None
    teamID = 0

    # initialisation attributes and established item:
    # commandSoldierList, heroUnit, teamID
    # collecting team, data will present in teamsList
    def __init__(self):
        global teamCounter
        print("Create Team")
        self.heroUnit = Hero()
        self.commandSoldierList = list()
        self.teamID = teamCounter
        teamCounter = teamCounter + 1

    @property
    def level(self):
        return len(self.commandSoldierList)

    def create_soldier(self):
        self.commandSoldierList.append(Soldier())

    def print(self):
        print("")
        print("Team ID {}".format(self.teamID))
        print("\tHero ID {}".format(self.heroUnit.unq_number))
        print("\tSoldier Count {}".format(len(self.commandSoldierList)))
        for soldier in self.commandSoldierList:
            print("\t\tSoldier ID {}".format(soldier.unq_number))


teamsList = list()

for i in range(teamsCount):
    element = Team()
    teamsList.append(element)


for i in range(soldierCount):
    teamID = random.randint(0, teamsCount - 1)
    teamsList[teamID].create_soldier()

# find list of soldiers with max len
# team with max list are searched by maxIndex


def get_max_level_team_id(list_1):
    if len(list_1) == 0:
        return None

    max_index = 0
    for index in range(len(list_1)):
        if list_1[index].level > list_1[max_index].level:
            max_index = index

    return max_index


print("Teams List")
for team in teamsList:
    team.print()

maxLevelTeamID = get_max_level_team_id(teamsList)

teamsList[maxLevelTeamID].heroUnit.level_up()

print("Hero to Level Up ID {}"
      .format(teamsList[maxLevelTeamID].heroUnit.unq_number))
if len(teamsList[0].commandSoldierList) != 0:
    teamsList[0].commandSoldierList[0]\
        .move_to_hero(teamsList[maxLevelTeamID].heroUnit)
    print("Soldier to Move ID {}"
          .format(teamsList[0].commandSoldierList[0].unq_number))
else:
    print("SoldierList is empty!")

"""Создать класс Goods, в котором содержиться атрибуты: price, discount.
и метод set_discount

Унаследовать класс Food и Tools от Goods
"""


class Goods:
    price = 100
    discount = 5

    def __init__(self, price, discount):
        self.price = price
        self.discount = discount

    @property
    def get_discount(self):
        return self.discount

    @get_discount.setter
    def get_discount(self, discount):
        self.discount = discount


class Food(Goods):
    def __init__(self, price, discount, expiration_date):
        super().__init__(price, discount)
        self.expiration_date = expiration_date


class Tools(Goods):
    def __init__(self, price, discount):
        super().__init__(price, discount)


"""Унаследовать класс Banana, Apple, Cherry от класса Food
Унаследовать класс Ham, Nail, Axe от класса Tools
"""


class Banana(Food):
    def __init__(self, price, discount, expiration_date):
        super().__init__(price, discount, expiration_date)


american_banana = Banana(200, 20, '20-20-2030')
print(american_banana.expiration_date)


class Apple(Food):
    def __init__(self, price, discount, expiration_date):
        super().__init__(price, discount, expiration_date)


american_Apple = Apple(400, 60, '20-20-2030')
print(american_Apple.price)


class Cherry(Food):
    def __init__(self, price, discount, expiration_date):
        super().__init__(price, discount, expiration_date)


american_Cherry = Cherry(100, 30, '20-20-2030')
print(american_Cherry.discount)


class Ham(Tools):
    def __init__(self, price, discount):
        super().__init__(price, discount)


american_ham = Ham(12, 20)
print(american_ham.price)


class Nail(Tools):
    def __init__(self, price, discount):
        super().__init__(price, discount)


american_nail = Nail(14, 15)
print(american_nail.price)


class Axe(Tools):
    def __init__(self, price, discount):
        super().__init__(price, discount)


american_axe = Ham(55, 5)
print(american_axe.discount)

"""Создать класс Store и добавить в него словарь,
 где будет храниться item: price.
Реализовать в классе методы add_item, remove_item
Реализовать метод overall_price_discount
 который считает полную сумму со скидкой
  добавленных в магазин товаров
Реализовать метод overall_price_no_discount
который считает сумму товаров без скидки.
"""


class Store(Goods):
    dict_1 = {}
    price = 0

    # class for adding/deleting items and price

    def __init__(self, item, price, discount):
        super().__init__(price, discount)
        self.item = item
        self.price = price

    def add_item(self):
        # add to dict like item: price
        list_item_price = [self.item, self.price]
        list_entry = iter(list_item_price)
        dict_1 = dict(zip(list_entry, list_entry))
        return dict_1

    def remove_item(self):
        self.item = "just remove"

    def overall_price_discount(self):
        return self.price * 0.01 * Goods.discount

    def overall_price_no_discount(self):
        return self.price


garlic = Store('garlic', 366, 20)
pineapple = Store('pineapple', 200, 20)

print(garlic.add_item())
print(pineapple.add_item())
print(garlic.overall_price_discount())
print(garlic.overall_price_no_discount())

"""
Реализовать классы GroceryStore и
HardwareStore и унаследовать эти классы от Store
Реализовать в GraceryStore проверку,
что объект, который был передан в класс пренадлежит классу Food
HardwareStore Tools
"""


class GroceryStore(Store):
    def __init__(self, price, discount):
        super().__init__(price, discount)


print(issubclass(GroceryStore, Food))


class HardwareStore(Store):
    def __init__(self, price,):
        super().__init__(price)


print(issubclass(HardwareStore, Tools))
