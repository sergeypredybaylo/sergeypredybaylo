import time


def timer(func):
    def wrapper(arg):
        start_time = time.perf_counter()
        func(arg)
        end_time = time.perf_counter()
        return end_time - start_time
    return wrapper


def get_avarage(amont_of_times=4):

    def get_avr(func):
        lst = []

        def inner_function(arg):
            for i in range(amont_of_times):
                result = func(arg)
                lst.append(result)
            result = sum(lst) / len(lst)
            print(f"Avarage result for {amont_of_times} times is: ", result)
            return result
        return inner_function
    return get_avr


@get_avarage(amont_of_times=6)
@timer
def gen_list_with_append(n):
    lst = []
    for el in range(n):
        lst.append(el)
    return lst


def gen_list_comprehention(n):
    lst = [x for x in range(n)]
    return lst


def decore(func):
    def wrapper(arg):
        result = func(arg)
        return result
    return wrapper


gen_list_with_append(10)
