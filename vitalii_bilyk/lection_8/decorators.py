import time
from functools import wraps


def decorate(func):
    def inner_function():
        x = 1
        print(x)
        func()
        print("The end of the function")
    return inner_function


def timer(func):
    def wrapper(arg):
        print("Start decoration...")
        start_time = time.perf_counter()
        func(arg)
        end_time = time.perf_counter()
        print("Executed in: ", end_time - start_time)
        print("End decoration...")
    return wrapper


def do_twice(func):
    @wraps(func)
    def wrapper_do_twice(*args, **kwargs):
        print(f"Function {func.__name__} is executed")
        func(*args, **kwargs)
        return func(*args, **kwargs)
    return wrapper_do_twice
