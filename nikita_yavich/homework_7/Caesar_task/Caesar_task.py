# This module contains function 'encode_decode' that has 2 args(input string
# and the letter shift) to encode or decode in a special way as Caesar did.
# The second function is my experimental function.It is easy to encode the
# string but impossible to decode in a fast way. I havent invented the
# logics to decode it yet.

# Шифр Цезаря — один из древнейших шифров. При шифровании каждый символ
# заменяется другим, отстоящим от него в алфавите на фиксированное
# число позиций.
# Примеры:
# hello world! -> khoor zruog!
# this is a test string -> ymnx nx f yjxy xywnsl
# Напишите две функции - encode и decode принимающие как параметр строку и
# число - сдвиг.
# Все взаимодействие с программой должно производиться через терминал!
# Привер:
# Выберите операцию:
# 1) Encode
# 2) Decode
# Введите фразу:
# И так далее!
# Бонусные баллы
# Шифр сохраняет исходный регистр и знаки препинания
# Можно ли обойтись одной функцией для зашифровки и дешифровки?
# Придумайте свой шифр

from string import ascii_lowercase as abc, ascii_uppercase as ABC


def encode_decode(input_string: str, shift: int):
    """
    This function takes 2 args:input string and the letter shift to encode or
     decode in a special way as Caesar did.
    """
    output_list = []
    for i in input_string:
        if i in abc:
            output_list.append((abc * 2)[abc.index(i) - len(abc) + shift])
        if i in ABC:
            output_list.append((ABC * 2)[ABC.index(i) - len(ABC) + shift])
        if i not in abc and i not in ABC:
            output_list.append(i)
    return ''.join(output_list)


def caesar_input_check():
    """
    This function allows to choose module launching options and checks the
    input data to be correct.
    """
    while True:
        try:
            print('Input the operation to do:', '1) Encode', '2) Decode',
                  sep='\n')
            choice = int(input())
            if choice in [1, 2]:
                break
            else:
                print('Wrong data input. Try again')
                continue
        except ValueError:
            print('Wrong data input. Try again')
    input_string = input('Input the string to code: ')
    while True:
        try:
            shift = int(input('Input the shift of coding(a number): '))
            if choice == 2:
                shift *= -1
            break
        except ValueError:
            print('Wrong data input. Try again')
    return input_string, shift


if __name__ == '__main__':
    input_string, shift = caesar_input_check()
    print('The final string: ', encode_decode(input_string, shift))

# My coding way:


# def my_code(input_string: str):
#     """
#     This function takes 1 arg:input string to encode in a special way as
#      I invented. Bad logics:it is easy to code but very hard to decode. I
#      havent tried.
#     """
#     output_list = []
#     aeiouy_low = 'aeiouy'
#     aeiouy_up = aeiouy_low.upper()
#     bcdfghjklmnpqrstvwxz_low = 'bcdfghjklmnpqrstvwxz'
#     bcdfghjklmnpqrstvwxz_up = bcdfghjklmnpqrstvwxz_low.upper()
#
#     for i in input_string:
#         if i in aeiouy_low:
#             output_list.append(
#                 (bcdfghjklmnpqrstvwxz_low * 2)
#                 [aeiouy_low.index(i) - len(bcdfghjklmnpqrstvwxz_low)]
#             )
#         if i in aeiouy_up:
#             output_list.append(
#                 (bcdfghjklmnpqrstvwxz_up * 2)
#                 [aeiouy_up.index(i) - len(bcdfghjklmnpqrstvwxz_up)]
#             )
#         if i not in aeiouy_low and i not in aeiouy_up:
#             output_list.append(i)
#     return ''.join(output_list)
#
#
# while True:
#     try:
#         print('Input the operation to do:', '1) Encode', '2) Decode',
#               sep='\n')
#         choice = int(input())
#         if choice in [1, 2]:
#             break
#         else:
#             print('Wrong data input. Try again')
#             continue
#     except ValueError:
#         print('Wrong data input. Try again')
# input_string = input('Input the string to code: ')
# if __name__ == '__main__':
#     print(my_code(input_string))
