from nikita_yavich.homework_18.classwork_11.classwork11 import Book, \
    PagesException, \
    YearException, \
    AuthorException, PriceException
import unittest.mock
import unittest


def mock_pages_validation(arg):
    return arg


class TestBookPagesValidationOK(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("Starting TestBookPagesValidation OK tests...")

    def test_pages_validation_right_input(self):
        self.assertEqual(Book.validate_pages(3999), 3999)

    @unittest.mock.patch(
        'nikita_yavich.homework_18.classwork_11.classwork11.'
        'Book.validate_pages',
        side_effect=mock_pages_validation)
    def test_pages_validation_right_input_mock(self, pages):
        self.assertEqual(Book.validate_pages(4500), 4500)

    @unittest.mock.patch(
        'nikita_yavich.homework_18.classwork_11.classwork11.'
        'Book.validate_pages',
        side_effect=mock_pages_validation)
    def test_pages_validation_mock(self, pages):
        book = Book(5000, 1985, 'Nabokov', 1300)
        self.assertEqual(book.pages, 5000)

    def test_pages_validation_wrong_output(self):
        self.assertNotEqual(Book.validate_pages(3999), 4000)

    def test_pages_validation_wrong_input(self):
        with self.assertRaises(PagesException):
            Book.validate_pages(4000)

    def test_pages_validation_string_input(self):
        with self.assertRaises(TypeError):
            Book.validate_pages('4000')

    def test_pages_validation_empty_input(self):
        with self.assertRaises(TypeError):
            Book.validate_pages()

    def test_pages_validation_more_args_input(self):
        with self.assertRaises(TypeError):
            Book.validate_pages(3500, 3300)

    @classmethod
    def tearDownClass(cls):
        print("Finishing TestBookPagesValidation OK tests...")


class TestBookPagesValidationFail(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("Starting TestBookPagesValidation Fail tests...")

    @unittest.expectedFailure
    def test_pages_validation_wrong_output(self):
        self.assertEqual(Book.validate_pages(3999), 4000)

    @unittest.expectedFailure
    def test_pages_validation_right_input(self):
        self.assertNotEqual(Book.validate_pages(3999), 3999)

    @unittest.expectedFailure
    def test_pages_validation_wrong_exception_1(self):
        with self.assertRaises(YearException):
            Book.validate_pages(4000)

    @unittest.expectedFailure
    def test_pages_validation_wrong_exception_2(self):
        with self.assertRaises(AuthorException):
            Book.validate_pages(4000)

    @unittest.expectedFailure
    def test_pages_validation_wrong_exception_3(self):
        with self.assertRaises(PriceException):
            Book.validate_pages(4000)

    @classmethod
    def tearDownClass(cls):
        print("Finishing TestBookPagesValidation Fail tests...")


class TestBookYearValidationOK(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("Starting TestBookYearValidation OK tests...")

    def test_year_validation_right_input(self):
        self.assertEqual(Book.validate_year(1981), 1981)

    def test_year_validation_wrong_output(self):
        self.assertNotEqual(Book.validate_year(1981), 1982)

    def test_year_validation_wrong_input(self):
        with self.assertRaises(YearException):
            Book.validate_year(1979)

    def test_year_validation_string_input(self):
        with self.assertRaises(TypeError):
            Book.validate_year('1979')

    def test_year_validation_empty_input(self):
        with self.assertRaises(TypeError):
            Book.validate_year()

    def test_year_validation_more_args_input(self):
        with self.assertRaises(TypeError):
            Book.validate_year(1979, 1981)

    @classmethod
    def tearDownClass(cls):
        print("Finishing TestBookYearValidation OK tests...")


class TestBookYearValidationFail(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("Starting TestBookYearValidation Fail tests...")

    @unittest.expectedFailure
    def test_year_validation_wrong_output(self):
        self.assertEqual(Book.validate_year(1979), 1981)

    @unittest.expectedFailure
    def test_year_validation_right_input(self):
        self.assertNotEqual(Book.validate_year(1981), 1981)

    @unittest.expectedFailure
    def test_year_validation_wrong_exception_1(self):
        with self.assertRaises(PagesException):
            Book.validate_year(1979)

    @unittest.expectedFailure
    def test_pages_validation_wrong_exception_2(self):
        with self.assertRaises(AuthorException):
            Book.validate_year(1979)

    @unittest.expectedFailure
    def test_pages_validation_wrong_exception_3(self):
        with self.assertRaises(PriceException):
            Book.validate_year(1979)

    @classmethod
    def tearDownClass(cls):
        print("Finishing TestBookYearValidation Fail tests...")


class TestBookAuthorValidationOK(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("Starting TestBookAuthorValidation OK tests...")

    def test_author_validation_right_input(self):
        self.assertEqual(Book.validate_author('Nabokov'), 'Nabokov')

    def test_author_validation_wrong_output(self):
        self.assertNotEqual(Book.validate_author('Nabokov'), None)

    def test_author_validation_wrong_input(self):
        with self.assertRaises(AuthorException):
            Book.validate_author('Nabokov1')

    def test_author_validation_int_input(self):
        with self.assertRaises(AttributeError):
            Book.validate_author(123)

    def test_author_validation_empty_input(self):
        with self.assertRaises(TypeError):
            Book.validate_author()

    def test_author_validation_more_args_input(self):
        with self.assertRaises(TypeError):
            Book.validate_author('Nabokov', 'Gogol')

    @classmethod
    def tearDownClass(cls):
        print("Finishing TestBookAuthorValidation OK tests...")


class TestBookAuthorValidationFail(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("Starting TestBookAuthorValidation Fail tests...")

    @unittest.expectedFailure
    def test_author_validation_wrong_output(self):
        self.assertEqual(Book.validate_author('Nabokov'), None)

    @unittest.expectedFailure
    def test_author_validation_right_input(self):
        self.assertNotEqual(Book.validate_author('Nabokov'), 'Nabokov')

    @unittest.expectedFailure
    def test_author_validation_wrong_exception_1(self):
        with self.assertRaises(YearException):
            Book.validate_author('Nabokov1')

    @unittest.expectedFailure
    def test_author_validation_wrong_exception_2(self):
        with self.assertRaises(PagesException):
            Book.validate_author('Nabokov1')

    @unittest.expectedFailure
    def test_author_validation_wrong_exception_3(self):
        with self.assertRaises(PriceException):
            Book.validate_author('Nabokov1')

    @classmethod
    def tearDownClass(cls):
        print("Finishing TestBookAuthorValidation Fail tests...")


class TestBookPriceValidationOK(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("Starting TestBookPriceValidation OK tests...")

    def test_price_validation_right_input_1(self):
        self.assertEqual(Book.validate_price(101), 101)

    def test_price_validation_right_input_2(self):
        self.assertEqual(Book.validate_price(9999), 9999)

    def test_price_validation_wrong_output(self):
        self.assertNotEqual(Book.validate_price(9999), None)

    def test_price_validation_wrong_input(self):
        with self.assertRaises(PriceException):
            Book.validate_price(10001)

    def test_price_validation_str_input(self):
        with self.assertRaises(TypeError):
            Book.validate_price('101')

    def test_price_validation_empty_input(self):
        with self.assertRaises(TypeError):
            Book.validate_price()

    def test_price_validation_more_args_input(self):
        with self.assertRaises(TypeError):
            Book.validate_price(123, 9999)

    @classmethod
    def tearDownClass(cls):
        print("Finishing TestBookPriceValidation OK tests...")


class TestBookPriceValidationFail(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("Starting TestBookPriceValidation Fail tests...")

    @unittest.expectedFailure
    def test_price_validation_wrong_output(self):
        self.assertEqual(Book.validate_price(101), None)

    @unittest.expectedFailure
    def test_price_validation_right_input(self):
        self.assertNotEqual(Book.validate_price(101), 101)

    @unittest.expectedFailure
    def test_price_validation_wrong_exception_1(self):
        with self.assertRaises(YearException):
            Book.validate_price(99)

    @unittest.expectedFailure
    def test_price_validation_wrong_exception_2(self):
        with self.assertRaises(PagesException):
            Book.validate_price(99)

    @unittest.expectedFailure
    def test_price_validation_wrong_exception_3(self):
        with self.assertRaises(AuthorException):
            Book.validate_price(99)

    @classmethod
    def tearDownClass(cls):
        print("Finishing TestBookPriceValidation Fail tests...")
