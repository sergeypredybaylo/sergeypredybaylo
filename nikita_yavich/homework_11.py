# На основании этих классов создать отдельные классы для Свиньи, Гуся,
# Курицы и Коровы – для каждого класса метод голос должен выводить разное
# сообщение (хрю, гага, кудах, му).
# После этого создать класс Ферма, в который можно передавать список животных
# Должна быть возможность получить каждое животное, живущее на ферме как по
# индексу, так и через цикл for.
# Также у фермы должен быть метод, который увеличивает возраст всех животных,
# живущих на ней.
class NotExistException(Exception):
    def __init__(self, message='Error'):
        """
        Class MyException constructor.
        """
        super(NotExistException, self).__init__(message)


class AnimalsAndBirds:
    def __getattr__(self, item):
        """
        Raises the special error while trying to use attribute or method
        that doesnt exist.
        """
        raise NotExistException(
            f'{self.__class__.__name__} has no such attribute or '
            f'method: '
            f' {item}')

    def __init__(self, name: str, _age: int):
        """
        Class Farm constructor.
        """
        self.name = name
        self._age = _age

    def become_older(self):
        """
        Become a bit older.
        """
        self._age += 1

    sound = 'Some quietly sounds from the yard..'

    def make_a_sound(self):
        print(f'{self.sound}')

    def __str__(self):
        return f"{self.name}"


class Animals(AnimalsAndBirds):
    def __init__(self, name, _age):
        """
        Class Animals constructor.
        """

        super().__init__(name, _age)

    def go(self):
        """
        Prints message for animals.
        """
        print('Lets go around the yard')


class Birds(AnimalsAndBirds):
    def __init__(self, name, _age):
        """
        Class Birds constructor.
        """

        super().__init__(name, _age)

    def fly(self):
        """
        Prints message for birds.
        """
        print('Lets fly around the yard')


class Pig(Animals):
    def __init__(self, name, _age):
        """
        Class Pig constructor.
        """
        super().__init__(name, _age)

    sound = 'oink!'


class Goose(Birds):
    def __init__(self, name, _age):
        """
        Class Goose constructor.
        """
        super().__init__(name, _age)

    sound = 'honk!'


class Chicken(Birds):
    def __init__(self, name, _age):
        """
        Class Chicken constructor.
        """
        super().__init__(name, _age)

    sound = 'Cluck!'


class Cow(Animals):
    def __init__(self, name, _age):
        """
        Class Cow constructor.
        """
        super().__init__(name, _age)

    sound = 'MOOOOOOOOOOOO!'


class Farm:
    def __init__(self, *animals_and_birds):
        """
        Class Farm constructor.
        """
        self.animals_and_birds = animals_and_birds

    def __iter__(self):
        """
        Gives opportunity to see all animals and birds using for-cycle.
        """
        return iter(self.animals_and_birds)

    def __getitem__(self, index):
        """
        Finds the animal or bird using index.
        """
        return self.animals_and_birds[index]

    def become_older_in_farm(self):
        """
        This method increases age of every unit on the farm.
        """
        for i in self.animals_and_birds:
            AnimalsAndBirds.become_older(i)
