import requests
import datetime


class Endpoints:
    AUTHORS = "Authors"
    AUTHOR = "Authors/{author_id}"
    BOOKS = "Books"
    BOOK = "Books/{id}"
    USERS = "Users"
    USER = "Users/{id}"


def check_response(response):
    response.raise_for_status()
    return response


class RestHelper:
    def __init__(self, url):
        self.url = url
        self.session = requests.Session()

    def do_get(self, endpoint):
        response = self.session.get(self.url + endpoint)
        return check_response(response).json()

    def do_post(self, endpoint, **kwargs):
        response = self.session.post(self.url + endpoint, **kwargs)
        return check_response(response).json()

    def do_put(self, endpoint, **kwargs):
        response = self.session.put(self.url + endpoint, **kwargs)
        return check_response(response).json()

    def do_delete(self, endpoint, **kwargs):
        response = self.session.delete(self.url + endpoint, **kwargs)
        return check_response(response)


class FakerestapiHelper(RestHelper):
    def __init__(self):
        self.url = "https://fakerestapi.azurewebsites.net/api/v1/"
        super().__init__(self.url)

    def get_authors(self):
        return self.do_get(Endpoints.AUTHORS)

    def get_books(self):
        return self.do_get(Endpoints.BOOKS)

    def get_author_by_id(self, author_id):
        return self.do_get(
            Endpoints.AUTHOR.format(author_id=author_id))

    def create_book(self, id, title, description, page_count, excerpt, date):
        book = {
            "id": id,
            "title": title,
            "description": description,
            "pageCount": page_count,
            "excerpt": excerpt,
            "publishDate": date
        }

        return self.do_post(
            Endpoints.BOOKS.format(), json=book)

    def create_user(self, id, username, password):
        user = {
            "id": id,
            "userName": username,
            "password": password,
        }

        return self.do_post(
            Endpoints.USERS.format(), json=user)

    def update_the_book(self, id, title, description, page_count, excerpt,
                        date):
        book = {
            "id": id,
            "title": title,
            "description": description,
            "pageCount": page_count,
            "excerpt": excerpt,
            "publishDate": date
        }

        return self.do_put(
            Endpoints.BOOK.format(id=id), json=book)

    def delete_user(self, id):
        return self.do_delete(Endpoints.USER.format(id=id))


if __name__ == "__main__":
    agent = FakerestapiHelper()
    print(agent.get_authors())
    print(agent.get_author_by_id(5))
    now = datetime.datetime.now()
    current_time = now.strftime("%Y-%m-%dT%H:%M:%S.%fZ")[:-3]
    agent.create_book(6666, 'Lolita', 'novel', 565, 'smth',
                      f'{current_time}')
    agent.create_user(99, 'nikita', 'yavich')
    now = datetime.datetime.now()
    current_time = now.strftime("%Y-%m-%dT%H:%M:%S.%fZ")[:-3]
    agent.update_the_book(10, 'Lolita', 'novel', 565, 'smth',
                          f'{current_time}')
    agent.delete_user(4)
