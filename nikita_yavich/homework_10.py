# Цветочница.
# Определить иерархию и создать несколько цветов (Rose, Tulips, violet,
# chamomile). У каждого класса цветка следующие атрибуты:
# Стоимость – атрибут класса, который определяется заранее
# Свежесть (в днях), цвет, длинна стебля – атрибуты экземпляра
# При попытке вывести информацию о цветке, должен отображаться цвет и тип
# Rose = RoseClass(7, Red, 15)
# Print(Rose) -> Red Rose (__str__)
# Собрать букет (можно использовать аксессуары – отдельные классы со своей
# стоимостью: например, упаковочная бумага) с определением ее стоимости.
# У букета должна быть возможность определить время его увядания по среднему
# времени жизни всех цветов в букете
# Позволить сортировку цветов в букете на основе различных параметров (
# свежесть/цвет/длина стебля/стоимость...)
# Реализовать поиск цветов в букете по определенным параметрам.
# Узнать, есть ли цветок в букете. (__contains__)
# Добавить возможность получения цветка по индексу (__getitem__) либо
# возможность пройтись по букету и получить каждый цветок по-отдельности (
# __iter__)


class Flowers:
    def __init__(self, name, as_fresh, color, lenght):
        """
        Class Flowers constructor.
        """
        self.name = name
        self.as_fresh = as_fresh
        self.color = color
        self.lenght = lenght

    def __str__(self):
        """
        Get information about instance in a right way.
        """
        return f'{self.color} {(self.__class__.__name__)}'


class Rose(Flowers):
    money = 10

    def __init__(self, name, as_fresh, color, lenght):
        super().__init__(name, as_fresh, color, lenght)


# Create flowers:
rose1 = Rose('rose1', 3, 'red', 25)
rose2 = Rose('rose2', 4, 'white', 22)
rose3 = Rose('rose3', 5, 'pink', 28)
# Get information about instance in a right way:
print(rose3)


class Tulip(Flowers):
    money = 7

    def __init__(self, name, as_fresh, color, lenght):
        super().__init__(name, as_fresh, color, lenght)


# Create flowers:
tulip1 = Tulip('tulip1', 5, 'white', 18)
tulip2 = Tulip('tulip2', 6, 'red', 25)
tulip3 = Tulip('tulip3', 3, 'yellow', 17)


class Violet(Flowers):
    money = 5

    def __init__(self, name, as_fresh, color, lenght):
        super().__init__(name, as_fresh, color, lenght)


# Create flowers:
violet1 = Violet('violet1', 5, 'white', 15)
violet2 = Violet('violet2', 12, 'violet', 16)
violet3 = Violet('violet3', 7, 'yellow', 21)


class Chamomile(Flowers):
    money = 3

    def __init__(self, name, as_fresh, color, lenght):
        super().__init__(name, as_fresh, color, lenght)


# Create flowers:
chamomile1 = Chamomile('chamomile1', 5, 'white', 18)
chamomile2 = Chamomile('chamomile2', 6, 'white', 22)
chamomile3 = Chamomile('chamomile3', 12, 'white', 15)


class Bouquet_paper:
    yellow_paper = 5
    green_paper = 3
    blue_paper = 2


class Bouqet:

    def __init__(self, *args, bouqet_paper):
        self.flowers = args
        if bouqet_paper == 'yellow':
            self.bouqet_paper = Bouquet_paper.yellow_paper
        if bouqet_paper == 'green':
            self.bouqet_paper = Bouquet_paper.green_paper
        if bouqet_paper == 'blue':
            self.bouqet_paper = Bouquet_paper.blue_paper
        if bouqet_paper != 'blue' and bouqet_paper != 'yellow' \
                and bouqet_paper != 'green':
            print('Sorry, there is no such color of paper. Lets try this '
                  'one!')
            self.bouqet_paper = Bouquet_paper.blue_paper

    def bouqet_price(self):
        price = self.bouqet_paper
        for i in self.flowers:
            price += i.money
        print(f'Bouqet costs {price} dollars.')

    def bouqet_life(self):
        days = 0
        for i in self.flowers:
            days += i.as_fresh
        print(f'Bouqet will look fresh {days / len(self.flowers)} days.')

    def sort_the_bouqet(self, parameter):
        self.parameter = parameter
        sort_list = []
        if self.parameter == 'fresh':
            for i in sorted(self.flowers, key=lambda x: x.as_fresh):
                sort_list.append(i.name)
            print(*sort_list)
        if self.parameter == 'color':
            for i in sorted(self.flowers, key=lambda x: x.color):
                sort_list.append(i.name)
            print(*sort_list)
        if self.parameter == 'lenght':
            for i in sorted(self.flowers, key=lambda x: x.lenght):
                sort_list.append(i.name)
            print(*sort_list)
        if self.parameter == 'money':
            for i in sorted(self.flowers, key=lambda x: x.money):
                sort_list.append(i.name)
            print(*sort_list)

    def find_a_flower(self, **kwargs):
        """
        Using few parameters you can find the flower in a bouqet.
        """
        self.parameters = kwargs
        if 'freshness' in self.parameters.keys():
            flower = list(filter(lambda x: x.as_fresh == self.parameters[
                'freshness'], self.flowers))
            for i in flower:
                print(i.name)
        if 'color' in self.parameters.keys():
            flower = list(filter(lambda x: x.color == self.parameters[
                'color'], self.flowers))
            for i in flower:
                print(i.name)
        if 'lenght' in self.parameters.keys():
            flower = list(filter(lambda x: x.lenght == self.parameters[
                'lenght'], self.flowers))
            for i in flower:
                print(i.name)
        if 'money' in self.parameters.keys():
            flower = list(filter(lambda x: x.money == self.parameters[
                'money'], self.flowers))
            for i in flower:
                print(i.name)

    def __contains__(self, x):
        """
        Checks if the flower is in a bouqet.
        """
        return x in self.flowers

    def __getitem__(self, index):
        """
        Finds the flower using index.
        """
        return self.flowers[index]

    def __iter__(self):
        """
        Iteration of flowers in bouqet.
        """
        return iter(self.flowers)


# Create bouqet:
bouqet = Bouqet(rose1, rose3, chamomile1, chamomile2, bouqet_paper='yellow')
# Calculate bouqet's price:
bouqet.bouqet_price()
# Calculate bouqet's life:
bouqet.bouqet_life()
# Sort the bouqet by freshness:
bouqet.sort_the_bouqet('fresh')
# Sort the bouqet by color:
bouqet.sort_the_bouqet('color')
# Sort the bouqet by lenght:
bouqet.sort_the_bouqet('lenght')
# Sort the bouqet by money:
bouqet.sort_the_bouqet('money')
# find a flower using  one of the parameters:
bouqet.find_a_flower(freshness=3)
bouqet.find_a_flower(color='red')
bouqet.find_a_flower(lenght=25)
bouqet.find_a_flower(money=3)
# check if a flower is in a boquet:
print(rose1 in bouqet)
# find a flower using  index:
print(bouqet.flowers[0])
# Iteration of flowers in bouqet:
for flower in bouqet:
    print(flower)
