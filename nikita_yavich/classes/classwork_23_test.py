import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By

BUTTON_CLICK_ME = "//a[@href='http://selenium2.ru']"
BUTTON_LOGIN = "//div[@class='loggedout menu']/a[" \
               "@href='https://jsbin.com/login']"


@pytest.fixture()
def driver():
    driver = webdriver.Chrome(
        executable_path=r"C:\\Users\\nikit\\Downloads\\chromedriver_win32"
                        "\\chromedriver.exe")
    driver.implicitly_wait(5)
    driver.get('https://jsbin.com/cicenovile/1/edit?html,output')
    yield driver
    driver.quit()


def test_frames_n_alerts(driver):
    main_window = driver.current_window_handle
    driver.switch_to.frame(driver.find_element(By.TAG_NAME, "iframe"))
    driver.switch_to.frame(driver.find_element(By.TAG_NAME, "iframe"))
    button_click_me = driver.find_element(By.XPATH, BUTTON_CLICK_ME)
    button_click_me.click()
    alert = driver.switch_to.alert
    alert.accept()
    driver.switch_to.window(main_window)
    button_login = driver.find_element(By.XPATH, BUTTON_LOGIN)
    assert "Login or Register" in button_login.text
