import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By


@pytest.fixture()
def driver():
    driver = webdriver.Chrome(
        executable_path=r"C:\\Users\\nikit\\Downloads\\chromedriver_win32"
                        "\\chromedriver.exe")
    driver.implicitly_wait(5)
    driver.get('https://www.saucedemo.com/')
    yield driver
    driver.quit()


@pytest.fixture()
def login(driver):
    username_bar = driver.find_element(By.XPATH, "//input[@id='user-name']")
    username_bar.send_keys("standard_user")
    password_bar = driver.find_element(By.XPATH, "//input[@id='password']")
    password_bar.send_keys("secret_sauce")
    login_button = driver.find_element(By.XPATH, "//input[@id='login-button']")
    login_button.click()


goods_select = (i for i in range(1, 7))


@pytest.fixture(params=goods_select)
def goods_selector(request):
    return request.param
