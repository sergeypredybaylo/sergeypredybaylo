import string


def is_special_character(arg: str):
    """
    Function checks if the arg is special character and
    returns True or False.
    """
    return (arg in string.punctuation)
