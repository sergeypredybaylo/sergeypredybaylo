import json

json_file = "students.json"

with open(json_file, "r") as f:
    file = f.read()


# Разработайте поиск учащихся в одном классе, посещающих одну секцию.

def get_students_of_the_same_class_club(st_class, club):
    for student in json.loads(file):
        if student["Class"] == st_class and student["Club"] == club:
            print(student["Name"])


get_students_of_the_same_class_club("3a", "Chess")


# Разработайте фильтрацию учащихся по их полу.

def get_student_name_by_gender(gender):
    for student in json.loads(file):
        if student["Gender"] == gender:
            print(student["Name"])


get_student_name_by_gender("W")


# Разработайте поиск ученика по имени(часть имени)

def get_student_id_by_name(name):
    for student in json.loads(file):
        if name in student["Name"]:
            print(student["ID"])


get_student_id_by_name("Yu")
