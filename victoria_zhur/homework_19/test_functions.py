import pytest
from check_case_func import check_lower_case
from convert_to_float_func import convert_to_float, InputTypeError
from check_special_character_func import check_special_character

"""
Для каждой функции создать параметризированный тест. В качестве параметров
тест принимает значение, которое должна обработать функция и ожидаемый
результат ее выполнения.
"""


@pytest.mark.parametrize("arg, expected", [("Vika", False), ("vika", True),
                                           ("vikA", False)])
def test_param_check_case_func(arg, expected):
    assert check_lower_case(arg) == expected


@pytest.mark.parametrize("arg, expected", [(1, 1.0), (0, 0.0), (10, 10.0)])
def test_param_convert_to_float(arg, expected):
    assert convert_to_float(arg) == expected


@pytest.mark.parametrize("arg, expected", [("*", True), ("aaa", False),
                                           (")", True)])
def test_param_check_special_character(arg, expected):
    assert check_special_character(arg) == expected


"""
Для 2-ой функции написать отдельный тест, который будет ожидать ошибку
InputTypeError, и если эта ошибка не произошла, падать!
"""


@pytest.mark.repeat(3)
@pytest.mark.parametrize("arg, expected", [(1.0, InputTypeError)])
def test_param_convert_to_float_incorrect_type(arg, expected):
    with pytest.raises(InputTypeError):
        assert convert_to_float(arg) == expected
