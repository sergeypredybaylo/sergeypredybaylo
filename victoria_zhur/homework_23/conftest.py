import pytest
from selenium import webdriver

URL_1 = "http://the-internet.herokuapp.com/dynamic_controls"
URL_2 = "http://the-internet.herokuapp.com/frames"


@pytest.fixture()
def driver_controls():
    driver = webdriver.Chrome()
    driver.get(URL_1)
    yield driver
    driver.quit()


@pytest.fixture()
def driver_frames():
    driver = webdriver.Chrome()
    driver.get(URL_2)
    yield driver
    driver.quit()
