import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException


"""
Dynamic Controls
Найти чекбокс
Нажать на кнопку
Дождаться надписи “It’s gone”
Проверить, что чекбокса нет
Найти инпут
Проверить, что он disabled
Нажать на кнопку
Дождаться надписи “It's enabled!”
Проверить, что инпут enabled
"""

# Locators
CHECKBOX = '//input[type="checkbox"]'
REMOVE_BUTTON = '//button[text()="Remove"]'
CHECKBOX_REMOVED_MESSAGE = '//p[text()[contains(.,"gone")]]'
INPUT = '//*[@id="input-example"]/input'
ENABLE_BUTTON = '//button[text()="Enable"]'
INPUT_ENABLED_MESSAGE = '//p[text()[contains(.,"enabled")]]'


def test_dynamic_controls(driver_controls):
    # нажать кнопку
    driver_controls.find_element(By.XPATH, REMOVE_BUTTON).click()
    # дождаться надписи “It’s gone”
    WebDriverWait(driver_controls, 5).until(EC.presence_of_element_located(
        (By.XPATH, CHECKBOX_REMOVED_MESSAGE)))
    # проверить, что чекбокса нет
    with pytest.raises(NoSuchElementException):
        driver_controls.find_element(By.XPATH, CHECKBOX)
    # проверить, что инпут disabled
    assert not driver_controls.find_element(By.XPATH, INPUT).is_enabled()
    # нажать кнопку
    driver_controls.find_element(By.XPATH, ENABLE_BUTTON).click()
    # дождаться надписи “It's enabled!”
    WebDriverWait(driver_controls, 5).until(EC.presence_of_element_located(
        (By.XPATH, INPUT_ENABLED_MESSAGE)))
    # проверить, что инпут enabled
    assert driver_controls.find_element(By.XPATH, INPUT).is_enabled()


"""
Открыть iFrame
Проверить, что текст внутри параграфа равен “Your content goes here.”
"""


def test_iframe(driver_frames):
    # открыть iframe
    driver_frames.find_element(By.LINK_TEXT, "iFrame").click()
    # проверить, что текст внутри параграфа равен “Your content goes here.”
    driver_frames.switch_to.frame(driver_frames.find_element(By.TAG_NAME,
                                                             "iframe"))
    iframe_text = driver_frames.find_element(By.TAG_NAME, "p").text
    assert iframe_text == "Your content goes here."
