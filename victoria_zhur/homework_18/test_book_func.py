from book_func import Book, YearError, PageAmountError, PriceError
from unittest.mock import patch
import unittest


def mock_author_name(author_name):
    return author_name


class TestBookYear(unittest.TestCase):

    def test_year_equal_to_boundary_value(self):
        self.assertEqual(Book.validate_year(1980), 1980)

    def test_year_greater_than_boundary_value_ok(self):
        self.assertEqual(Book.validate_year(1981), 1981)

    def test_year_less_than_boundary_value_fail(self):
        with self.assertRaises(YearError) as error:
            Book.validate_year(1979)
        self.assertEqual(error.exception.message,
                         "Year should not be less than 1980")


class TestBookPagesAmount(unittest.TestCase):

    def test_pages_amount_equal_to_boundary_value(self):
        self.assertEqual(Book.validate_pages(4000), 4000)

    def test_pages_amount_less_than_boundary_value_ok(self):
        self.assertEqual(Book.validate_pages(3999), 3999)

    def test_pages_greater_than_boundary_value_fail(self):
        with self.assertRaises(PageAmountError) as error:
            Book.validate_pages(4001)
        self.assertEqual(error.exception.message,
                         "Page amount should not be greater than 4000")


class TestBookPrice(unittest.TestCase):

    def test_price_is_equal_to_bottom_boundary(self):
        self.assertEqual(Book.validate_price(100), 100)

    def test_price_is_equal_to_top_boundary(self):
        self.assertEqual(Book.validate_price(10000), 10000)

    def test_price_is_0(self):
        with self.assertRaises(PriceError):
            Book.validate_price(0)

    def test_price_less_than_bottom_boundary(self):
        with self.assertRaises(PriceError):
            Book.validate_price(99)

    def test_price_greater_than_top_boundary(self):
        with self.assertRaises(PriceError):
            Book.validate_price(10001)


class TestBookAuthor(unittest.TestCase):

    @patch("test_book_func.Book.validate_author",
           side_effect=mock_author_name)
    def test_to_check_mocked_author_name(self):
        self.assertEqual(Book.validate_author("Author112"), "Author112")
