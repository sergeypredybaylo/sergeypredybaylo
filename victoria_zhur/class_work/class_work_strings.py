"""Свяжите переменную с любой строкой, состоящей не менее чем из 8
символов. Извлеките из строки первый символ, затем последний, третий с
начала и третий с конца. Измерьте длину вашей строки. """

txt = "qwertyuiop"

print(txt[0])  # 1st symbol
print(txt[-1])  # last symbol
print(txt[2])  # 3rd symbol
print(txt[-3])  # 3rd from the end symbol
print(len(txt))  # length


"""Присвойте произвольную строку длиной 10-15 символов переменной и
извлеките из нее следующие срезы:"""

txt_1 = "pifgtyhdkeltyu"

# первые восемь символов
print(txt_1[0:8])
# четыре символа из центра строки
middle = int((len(txt_1) / 2))  # middle of the string
print(txt_1[middle - 2:middle + 2])  # 4 symbols from the centre
# символы с индексами кратными трем
print(txt_1[::3])
# переверните строку
print(txt_1[::-1])


"""Есть строка: “my name is name”. Напечатайте ее, но вместо 2ого “name”
вставьте ваше имя."""

txt_2 = "my name is name"
my_name = "Vika"

print(txt_2[:-4] + my_name)


"""Есть строка: test_string = "Hello world!", необходимо"""

text_string = "Hello world"

# напечатать на каком месте находится буква w
print(text_string.index("w"))
# кол-во букв l
print(text_string.count("l"))
# Проверить начинается ли строка с подстроки: “Hello”
print(text_string.startswith("Hello"))
# Проверить заканчивается ли строка подстрокой: “qwe”
print(text_string.endswith("qwe"))
