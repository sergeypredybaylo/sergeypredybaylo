import pytest
from selenium.webdriver.common.by import By


MAIN_PAGE_URL = "http://the-internet.herokuapp.com/"

"""
Задание 1
На главной странице перейти по ссылке Checkboxes
Установить галочку для второго чекбокса и проверить что она установлена
Для первого чекбокса убрать галочку и протестировать, что ее нет
"""


def test_checkbox(web, link):
    web.find_element(By.LINK_TEXT, "Checkboxes").click()
    checkbox1 = web.find_element(By.XPATH, '//*[@id="checkboxes"]/input[1]')
    checkbox2 = web.find_element(By.XPATH, '//*[@id="checkboxes"]/input[2]')
    if checkbox1.is_selected():  # clear box
        checkbox1.click()
    elif checkbox2.is_selected():
        checkbox2.click()
    checkbox2.click()
    assert checkbox2.is_selected()
    assert not checkbox1.is_selected()


"""
Задание 2
На главной странице перейти по ссылке Multiple Windows
После этого найти и нажать на ссылку Click here
Убедиться, что открылось новое окно и проверить что заголовок страницы
– New window
"""


def test_multiple(web, link):
    web.find_element(By.LINK_TEXT, "Multiple Windows").click()
    web.find_element(By.LINK_TEXT, "Click Here").click()
    web.switch_to.window(web.window_handles[1])
    assert web.title == "New Window"


"""
Задание 3
На главной странице перейти по ссылке Add/Remove elements
Проверить что при нажатии на кнопку Add element, появляется новый элемент
Написать отдельный тест, который проверяет удаление элементов
"""


def test_element_add(web, link):
    web.find_element(By.LINK_TEXT, "Add/Remove Elements").click()
    web.find_element(By.CSS_SELECTOR, 'button[onclick="addElement()"]').click()
    element = web.find_element(By.XPATH, '//*[@id="elements"]/button')
    assert element.is_displayed()


@pytest.mark.xfail
def test_element_remove(web, link):
    web.find_element(By.LINK_TEXT, "Add/Remove Elements").click()
    web.find_element(By.CSS_SELECTOR, 'button[onclick="addElement()"]').click()
    web.find_element(By.CSS_SELECTOR,
                     'button[onclick="deleteElement()"]').click()
    element = web.find_element(By.XPATH, '//*[@id="elements"]/button')
    assert element.is_displayed()
