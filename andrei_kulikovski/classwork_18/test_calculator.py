from calculator import find_sum, find_diff, find_multiple, find_div
import unittest


class TestCalculatorSum(unittest.TestCase):
    def test_sum_positive(self):
        answer = find_sum(10, 4)
        self.assertEqual(answer, 14)

    def test_sum_negative(self):
        answer = find_sum(-2, -4)
        self.assertEqual(answer, -6)

    @unittest.expectedFailure
    def test_sum_fail(self):
        answer = find_sum(2, 4)
        self.assertNotEqual(answer, 6)


class TestCalculatorDiff(unittest.TestCase):
    def test_diff_positive(self):
        answer = find_diff(8, 2)
        self.assertEqual(answer, 6)

    def test_diff_negative(self):
        answer = find_diff(-4, -2)
        self.assertEqual(answer, -2)

    @unittest.expectedFailure
    def test_diff_fail(self):
        answer = find_diff(4, 2)
        self.assertNotEqual(answer, 2)


class TestCalculatorMultiple(unittest.TestCase):
    def test_multiple_positive(self):
        answer = find_multiple(5, 2)
        self.assertEqual(answer, 10)

    def test_multiple_negative(self):
        answer = find_multiple(-4, -2)
        self.assertEqual(answer, 8)

    @unittest.expectedFailure
    def test_multiple_fail(self):
        answer = find_multiple(4, 2)
        self.assertNotEqual(answer, 8)


class TestCalculatorDiv(unittest.TestCase):
    def test_div_positive(self):
        answer = find_div(6, 2)
        self.assertEqual(answer, 3)

    def test_div_negative(self):
        answer = find_div(-4, -2)
        self.assertEqual(answer, 2)

    @unittest.expectedFailure
    def test_div_fail(self):
        answer = find_div(4, 2)
        self.assertNotEqual(answer, 2)
