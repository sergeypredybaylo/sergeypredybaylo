from book import Book, PageNumberError, YearError, AuthorError, PriceError
import unittest
from unittest.mock import patch


def mock_price(price):
    return price


class TestPrice(unittest.TestCase):
    @patch('test_book.Book.price_validation', side_effect=mock_price)
    def test_price(self, arg):
        self.assertEqual(Book.price_validation(99), 99)


class TestBookPage(unittest.TestCase):

    def test_page_num_validation_ok(self):
        self.assertEqual(Book.page_num_validation(4000), 4000)

    @unittest.expectedFailure
    def test_page_num_validation_fail(self):
        self.assertEqual(Book.page_num_validation(4001), 4001)

    def test_page_error(self):
        with self.assertRaises(PageNumberError):
            Book.page_num_validation(4001)


class TestBookYear(unittest.TestCase):

    def test_year_validation_ok(self):
        self.assertEqual(Book.year_validation(1980), 1980)

    @unittest.expectedFailure
    def test_year_validation_fail(self):
        self.assertEqual(Book.year_validation(1979), 1979)

    def test_year_error(self):
        with self.assertRaises(YearError):
            Book.year_validation(1979)


class TestBookAuthor(unittest.TestCase):

    def test_author_validation_ok(self):
        self.assertEqual(Book.author_validation("rrr"), "rrr")

    @unittest.expectedFailure
    def test_author_validation_fail(self):
        self.assertEqual(Book.author_validation("111"), "111")

    def test_author_error(self):
        with self.assertRaises(AuthorError):
            Book.author_validation("111")


class TestBookPrice(unittest.TestCase):

    def test_price_validation_ok(self):
        self.assertEqual(Book.price_validation(100), 100)

    @unittest.expectedFailure
    def test_price_validation_fail(self):
        self.assertEqual(Book.price_validation(99), 99)

    def test_price_error(self):
        with self.assertRaises(PriceError):
            Book.price_validation(99)
