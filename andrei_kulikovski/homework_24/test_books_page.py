from pages.books_page import BooksPage


def test_books_page(driver):
    page = BooksPage(driver)
    page.open_page()
    assert page.get_book_sub().text == "Books"
    assert driver.title == "Books | Oscar - Sandbox"
    assert "/books_2/" in driver.current_url


def test_open_book(driver):
    book_title = "Coders at Work"
    book_price = "£19.99"

    page = BooksPage(driver)
    page.open_page()
    found_book = page.open_book(book_title)
    assert book_title in found_book.title()
    assert found_book.img().is_displayed
    assert found_book.name() == book_title
    assert found_book.price() == book_price
