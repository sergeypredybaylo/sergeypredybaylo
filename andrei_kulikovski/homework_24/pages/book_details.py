from pages.base_page import BasePage
from pages.book_details_locators import BookDetailLocators


class BookDetailPage(BasePage):

    def __init__(self, driver, url):
        super().__init__(driver, url)

    def img(self):
        return self.driver.find_element(*BookDetailLocators.BOOK_IMG)

    def name(self):
        return self.driver.find_element(*BookDetailLocators.BOOK_NAME).text

    def price(self):
        return self.driver.find_element(*BookDetailLocators.BOOK_PRICE).text

    def title(self):
        return self.driver.title
