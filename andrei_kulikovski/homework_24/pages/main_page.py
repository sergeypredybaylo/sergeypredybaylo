from pages.base_page import BasePage
from pages.main_page_locators import MainPageLocators


class MainPage(BasePage):
    URL = "http://selenium1py.pythonanywhere.com/en-gb"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_main_sub(self):
        return self.driver.find_element(*MainPageLocators.MAIN_PAGE_SUB)
