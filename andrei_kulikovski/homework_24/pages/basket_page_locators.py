from selenium.webdriver.common.by import By


class BasketPageLocators:
    BASKET_PAGE_SUB = (By.XPATH, '//*[@class="page-header action"]/h1')
    BASKET_BUTTON = (By.XPATH, '//*[@class="btn-group"]/a')
    BASKET_INNER = (By.XPATH, '//*[@id="content_inner"]/p')
