from pages.base_page import BasePage
from pages.basket_page_locators import BasketPageLocators


class BasketPage(BasePage):
    URL = "http://selenium1py.pythonanywhere.com/en-gb/basket/"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_basket_sub(self):
        return self.driver.find_element(*BasketPageLocators.BASKET_PAGE_SUB)

    def open_basket_button(self):
        button = self.driver.find_element(*BasketPageLocators.BASKET_BUTTON)
        button.click()

    def basket_inner(self):
        inner = self.driver.find_element(*BasketPageLocators.BASKET_INNER)
        return inner
