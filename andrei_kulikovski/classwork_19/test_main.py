from andrei_kulikovski.classwork_19.main import sum_
import pytest


def test_sum():
    assert sum_([1, 2, 3]) == 6, "Should be 6"


@pytest.mark.skip
def test_sum_tuple():
    assert sum_((1, 2, 2)) == 6, "Should be 6"
