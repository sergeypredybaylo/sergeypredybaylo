def before_and_after(func):
    def wrapper(*args):
        print("Before")
        func(*args)
        print("After")
    return wrapper


def decor_sum(func):
    def wrapper(*args):
        return func(*args) + 1
    return wrapper


def decor_upper(func):
    def wrapper(arg):
        return func(arg.upper())
    return wrapper


def func_name(func):
    def wrapper(*args):
        print(func.__name__)
        return func(*args)
    return wrapper


def change(func):
    def wrapper(*args):
        return func(*args[::-1])
    return wrapper


def timer(func):
    import time

    def wrapper(*args):
        start = time.perf_counter()
        return_value = func(*args)
        end = time.perf_counter()
        print(f"Время выполнения: {end - start}")
        return return_value
    return wrapper


def typed(type='str'):
    def replace(func):
        def wrapped(*args):
            new_args = tuple()
            if type == 'int':
                for i in args:
                    if not isinstance(i, int):
                        i = float(i)
                    new_args += (i,)
            elif type == 'str':
                for i in args:
                    if not isinstance(i, str):
                        i = str(i)
                    new_args += (i,)
            return func(*new_args)
        return wrapped
    return replace
