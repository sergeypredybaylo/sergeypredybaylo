from homework_8 import func_text, func_x, func_hello, func_4, div, dive, fib,\
    func_mat, add_two_symbols, add_three_symbols


def select():
    while True:
        var_mod = int(input("1, 2, 3, 4, 5, 6, 7, 8, 9, 0 (exit): "))
        if var_mod == 1:
            func_text()
        elif var_mod == 2:
            print(func_x(5))
        elif var_mod == 3:
            print(func_hello("hello"))
        elif var_mod == 4:
            print(func_4())
        elif var_mod == 5:
            print(div(2, 4))
        elif var_mod == 6:
            print(dive(2, 4))
        elif var_mod == 7:
            fib()
        elif var_mod == 8:
            func_mat(6, 5)
        elif var_mod == 9:
            print(add_two_symbols("3", 5), add_two_symbols(5, 5),
                  add_two_symbols('a', 'b'), add_three_symbols(5, 6, 7),
                  add_three_symbols("3", 5, 0),
                  add_three_symbols(0.1, 0.2, 0.4))
        elif var_mod == 0:
            exit()


select()
