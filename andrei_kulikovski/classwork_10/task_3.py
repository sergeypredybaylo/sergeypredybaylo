"""
Создать класс Goods, в котором содержиться атрибуты: price, discount.
и метод set_discount
Унаследовать класс Food и Tools от Goods
Унаследовать класс Banana, Apple, Cherry от класса Food
Унаследовать класс Ham, Nail, Axe от класса Tools
Создать класс Store и добавить в него словарь, где будет храниться item: price.
Реализовать в классе методы add_item, remove_item
Реализовать метод overall_price_discount который считает полную сумму со
скидкой добавленных в магазин товаров
Реализовать метод overall_price_no_discount
который считает сумму товаров без скидки.
Реализовать классы GroceryStore и HardwareStore и унаследовать эти классы
от Store
Реализовать в GraceryStore проверку, что объект, который был передан в класс
пренадлежит классу Food
HardwareStore Tools
"""


class Goods:
    def __init__(self, price, discount):
        self.price = price
        self.discount = discount

    def set_discount(self):
        return self.discount


class Food(Goods):
    def __init__(self, price, discount):
        super().__init__(price, discount)


class Tools(Goods):
    def __init__(self, price, discount):
        super().__init__(price, discount)


class Banana(Food):
    def __init__(self, price, discount):
        super().__init__(price, discount)


class Apple(Food):
    def __init__(self, price, discount):
        super().__init__(price, discount)


class Cherry(Food):
    def __init__(self, price, discount):
        super().__init__(price, discount)


class Ham(Tools):
    def __init__(self, price, discount):
        super().__init__(price, discount)


class Nail(Tools):
    def __init__(self, price, discount):
        super().__init__(price, discount)


class Axe(Tools):
    def __init__(self, price, discount):
        super().__init__(price, discount)


class Store:
    items = {}

    def __init__(self, item, price):
        self.item = item
        self.price = price
        self.add_item()

    def add_item(self):
        Store.items.update({self.item: self.price})

    def remove_item(self):
        Store.items.pop(self.item)

    @classmethod
    def overall_price_discount(self):
        s = sum(Store.items.values()) - sum(Store.items.values()) * 0.15
        return s

    @classmethod
    def overall_price_no_discount(self):
        s = sum(self.items.values())
        return s


if __name__ == "__main__":
    print(f"task_3 is {__name__}")
else:
    print(f"import {__name__}")
axe = Store('axe', 300)
banana = Store('banana', 150)
axe.add_item()
banana.add_item()
print(Store.items)
print(Store.overall_price_discount())
print(Store.overall_price_no_discount())


class GroceryStore(Store):
    def __init__(self, item, price):
        super().__init__(item, price)


banana = GroceryStore('banana', 150)
print(issubclass(GroceryStore, Food))


class HardwareStore(Store):
    def __init__(self, item, price):
        super().__init__(item, price)


axe = HardwareStore('axe', 300)
print(issubclass(HardwareStore, Tools))
