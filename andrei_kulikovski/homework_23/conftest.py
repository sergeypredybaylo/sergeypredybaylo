import pytest
from selenium import webdriver


URL = 'http://the-internet.herokuapp.com/'


@pytest.fixture()
def driver():
    driver = webdriver.Chrome(executable_path="D:/QA python/Python/"
                                              "chromedriver")
    driver.get(URL)
    yield driver
    driver.quit()
