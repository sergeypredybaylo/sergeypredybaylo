import requests


class Endpoints:
    AUTHORS = "Authors"
    AUTHORS_ID = "Authors/{author_id}"
    BOOKS = "Books"
    BOOKS_ID = "Books/{book_id}"
    USERS = "Users"
    USERS_ID = "Users/{user_id}"


def check_response(response):
    response.raise_for_status()
    return response


class RestBase:
    def __init__(self, url):
        self.url = url
        self.session = requests.Session()

    def do_get(self, endpoint):
        response = self.session.get(self.url + endpoint)
        return check_response(response)

    def do_post(self, endpoint, **kwargs):
        response = self.session.post(self.url + endpoint, **kwargs)
        return check_response(response)

    def do_put(self, endpoint, **kwargs):
        response = self.session.put(self.url + endpoint, **kwargs)
        return check_response(response)

    def do_delete(self, endpoint):
        response = self.session.delete(self.url + endpoint)
        return check_response(response)


class AzureRest(RestBase):
    def __init__(self, api_version="v1"):
        self.api_version = api_version
        self.url = f"https://fakerestapi.azurewebsites.net/api/" \
                   f"{self.api_version}/"
        super().__init__(self.url)

    def get_authors(self):
        return self.do_get(Endpoints.AUTHORS)

    def get_author_id(self, author_id="101"):
        return self.do_get(Endpoints.AUTHORS_ID.format(author_id=author_id))

    def post_book(self, id=222, title="Book 222", description="More books",
                  pageCount=1200, excerpt="More books",
                  publishDate="2022-02-18T17:22:00.161Z"):
        data = {
            "id": id,
            "title": title,
            "description": description,
            "pageCount": pageCount,
            "excerpt": excerpt,
            "publishDate": publishDate
        }
        return self.do_post(Endpoints.BOOKS, json=data)

    def post_user(self, id=151547, userName="Andrei", password="password"):
        data = {
            "id": id,
            "userName": userName,
            "password": password
        }
        return self.do_post(Endpoints.USERS, json=data)

    def put_book(self, book_id="10", id=0, title="Book 0", description="book",
                 pageCount=200, excerpt="More books",
                 publishDate="2022-02-18T17:22:00.161Z"):
        data = {
            "id": id,
            "title": title,
            "description": description,
            "pageCount": pageCount,
            "excerpt": excerpt,
            "publishDate": publishDate
        }
        return self.do_put(Endpoints.BOOKS_ID.format(book_id=book_id),
                           json=data)

    def delete_user(self, user_id="4"):
        return self.do_delete(Endpoints.USERS_ID.format(user_id=user_id))


if __name__ == '__main__':
    print("Получение списка авторов", AzureRest().get_authors())
    print("Получение конкретного автора по его id", AzureRest().
          get_author_id())
    print("Добавить новую книгу", AzureRest().post_book())
    print("Добавить нового пользователя", AzureRest().post_user())
    print("Обновить данные для книги под номером 10", AzureRest().put_book())
    print("Удалить пользователя под номером 4", AzureRest().delete_user())
