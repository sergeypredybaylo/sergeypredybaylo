""""1. Перевести строку в массив
"Robin Singh" => ["Robin”, “Singh"]
"I love arrays they are my favorite" =>
["I", "love", "arrays", "they", "are", "my", "favorite"]"""
str_one = "Robin Singh"
str_two = "I love arrays they are my favorite"
str_one = str_one.split()
print(str_one)
str_two = str_two.split()
print(str_two)

""""2. Дан список: [‘Ivan’, ‘Ivanou’], и 2 строки: Minsk, Belarus
Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”"""
lst = ['Ivan', 'Ivanou']
str_one = 'Minsk'
str_two = 'Belarus'
lst = (' '.join(lst))
text = f'Привет, {lst}! Добро пожаловать в {str_one} {str_two}'
print('Задание 2:', text)

"""3. Дан список ["I", "love", "arrays", "they", "are", "my", "favorite"]
сделайте из него строку => "I love arrays they are my favorite"""
lst = ["I", "love", "arrays", "they", "are", "my", "favorite"]
str_one = (' '.join(lst))
print('Задание 3:', str_one)

"""4. Создайте список из 10 элементов, вставьте на 3-ю позицию новое
значение, удалите элемент из списка под индексом 6"""
lst = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
lst[3] = 'x'
del lst[6]
print('Задание 4', lst)

"""5. Есть 2 словаря
a = { 'a': 1, 'b': 2, 'c': 3}
b = { 'c': 3, 'd': 4,'e': “”}"""
"""5.1  Создайте словарь, который
будет содержать в себе все элементы обоих словарей"""
a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': ''}
c = {**a, **b}
print('Задание 5.1', c)

"""5.2 Обновите словарь “a” элементами из словаря “b”"""
new_a = a.copy()
new_a.update(b)
print('Задание 5.2', new_a)


"""5.3 Проверить что все значения в словаре “a”
не пустые либо не равны нулю"""
print(any(value == 0 for value in a.values()))
print('Задание 5.3', new_a)

"""6. Создать список из элементов list_a = [1,2,3,4,4,5,2,1,8]
a) Вывести только уникальные значения и сохранить их в отдельную переменную
b) Добавить в полученный объект значение 22
c) Сделать list_a неизменяемым
d) Измерить его длинну"""
print('Задание 6')
list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]
list_b = set(list_a)
print('a)', list_b)
list_b.add(20)
print('b)', list_b)
tuple(list_a)
print('c)', list_a)
print('d)', len(list_a))

"""Есть переменные a=10, b=25
Вывести, 'Summ is <сумма этих чисел> and diff = <их разница>.'"""
a = 10
b = 25
sum_1 = a + b
dif = a - b
print(f'Sum is {sum_1}, and diff = {dif}')

"""Есть список list_of_children = [“Sasha”, “Vasia”, “Nikalai”]
Вывести “First child is <первое имя из списка>, second is “<второе>”,
and last one – “<третье>”"""
list_of_children = ['Sasha', 'Vasia', 'Nikalai']
name_1 = list_of_children[0]
name_2 = list_of_children[1]
name_3 = list_of_children[2]
print(f'First child is {name_1}, second is {name_2}, and last one - {name_3}')
