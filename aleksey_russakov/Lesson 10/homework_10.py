from statistics import mean


# Цветочница
# Определить иерархию и создать несколько цветов (Rose, Tulips, violet,
# chamomile). У каждого класса цветка следующие атрибуты:
# Стоимость – атрибут класса, который определяется заранее
# Свежесть (в днях), цвет, длинна стебля – атрибуты экземпляра
# При попытке вывести информацию о цветке, должен отображаться цвет и тип
# Rose = RoseClass(7, Red, 15)

class Flower:
    def __init__(self, freshness, color, stalk_length):
        self.freshness = freshness
        self.color = color
        self.stalk_length = stalk_length

    def __str__(self):
        return f'{self.color}, {type(self).__name__}'


class Rose(Flower):
    price = 20


class Violet(Flower):
    price = 5


class Chamomile(Flower):
    price = 7


class Tulips(Flower):
    price = 12


# Собрать букет (можно использовать аксессуары – отдельные классы со своей
# стоимостью: например, упаковочная бумага) с определением ее стоимости.
# Узнать, есть ли цветок в букете. (__contains__)
# Добавить возможность получения цветка по индексу (__getitem__) либо
# возможность пройтись по букету и получить каждый цветок
# по-отдельности (__iter__)
class Bouquet:
    def __init__(self, *flowers):
        self.price = sum([arg.price for arg in flowers])
        self.flowers = flowers

        # У букета должна быть возможность определить время его увядания по
        # среднему времени жизни всех цветов в букете
        self.fade = mean([arg.freshness for arg in flowers])

    def __getitem__(self, index):
        return self.flowers[index]

    def __contains__(self, obj):
        return obj in self.flowers

    def __iter__(self):
        return iter(self.flowers)

# Позволить сортировку цветов в букете на основе различных параметров
# (свежесть/цвет/длина стебля/стоимость...)
    def sort(self, choice):

        if choice == '1':
            self.flowers = sorted(self.flowers, key=lambda x: x.freshness)

        elif choice == '2':
            self.flowers = sorted(self.flowers, key=lambda x: x.color)

        elif choice == '3':
            self.flowers = sorted(self.flowers, key=lambda x: x.stalk_length)

        elif choice == '4':
            self.flowers = sorted(self.flowers, key=lambda x: x.price)
        else:
            print('Choice must be 1 or 2 or 3 or 4')
            return
        list_flowers = []
        for i in self.flowers:
            list_flowers.append(str(i))
        return list_flowers

# Реализовать поиск цветов в букете по определенным параметрам.
    def search(self, choice, search_criteria):

        if choice == '1':
            criteria = int(search_criteria)
            for el in self.flowers:
                if criteria == el.freshness:
                    return el
            else:
                return 'no such flower'

        elif choice == '2':
            for el in self.flowers:
                if search_criteria == el.color:
                    return el
            else:
                return 'no such flower'
        elif choice == '3':
            criteria = int(search_criteria)
            for el in self.flowers:
                if criteria == el.stalk_length:
                    return el
            else:
                return 'no such flower'
        elif choice == '4':
            criteria = int(search_criteria)
            for el in self.flowers:
                if search_criteria == el.price:
                    return el
            else:
                return 'no such flower'


if __name__ == '__main__':
    rose = Rose(100, 'red', 100)
    print(rose)
    tulips = Tulips(22, 'blue', 30)
    tulips_1 = Tulips(21, 'black', 30)
    bouquet = Bouquet(rose, tulips, tulips_1)

    print(f'bouquet price (20 + 12 + 12) = {bouquet.price}')
    print(f'bouquet fade (100 + 22 + 21)/3 = {bouquet.fade}')

    choice = input('Choose method to sort:\n1. freshness\n2. color\n'
                   '3. stalk length\n4. price\n')
    print(bouquet.sort(choice))
    choice = input('Choose parameter to search for:\n1. freshness'
                   '\n2. color\n3. stalk length\n4. price\n')
    search_term = input('Search term: ')
    print(bouquet.search(choice, search_term))
    bouquet_2 = Bouquet(rose, tulips, tulips_1)
    print(f'rose in bouquet_2: {rose in bouquet_2}')
    print(f'item[0] in bouquet_2: {bouquet_2[0]}')
    print(f'item[1] in bouquet_2: {bouquet_2[1]}')
    for flowers in bouquet_2:
        print(flowers, end=";   ")
