import random
from math import sqrt

# 1. Заменить символ “#” на символ “/” в строке 'www.my_site.com#about'
str1 = 'www.my_site.com#about'
print('Task 1:', str1.replace('#', '/'))


# 2. В строке “Ivanou Ivan” поменяйте местами слова => "Ivan Ivanou"
str2 = 'Ivanou Ivan'
str2 = ' '.join(str2.split(' ')[::-1])
print('Task 2:', str2)


# 3. Напишите программу которая удаляет пробел в начале строки
str3 = " BruxBel"
str3 = str3.lstrip()
print('Task 3:', str3)


# 4. Напишите программу которая удаляет пробел в конце строки
str4 = "BruxBel "
str4 = str4.rstrip()
print('Task 4:', str4)


# 5. a = 10, b=23, поменять значения местами, чтобы в переменную “a” было
# записано значение “23”, в “b” - значение “-10”
a = 10
b = 23
a, b = b, -a
print(f'Task 5: a = {a}, b = {b}')


# 6. значение переменной “a” увеличить в 3 раза, а значение “b” уменьшить на
# 3 -------------
a *= 3
b /= 3
print(f'Task 6: a = {a}, b = {b}')


# 7. преобразовать значение “a” из целочисленного в число с плавающей точкой
# (float), а значение в переменной “b” в
# строку
a = float(a)
b = str(b)
print(f'Task 7: type a = {type(a)}, type b = {type(b)}')


# 8. Разделить значение в переменной “a” на 11 и вывести результат с точностью
# 3 знака после запятой
a /= 11
print('Task 8: a = %.3f' % a)


# 9. Преобразовать значение переменной “b” в число с плавающей точкой и
# записать в переменную “c”. Возвести полученное
# число в 3-ю степень.
c = float(b)
c **= 3
print('Task 9: c =', c)


# 10. Получить случайное число, кратное 3-м
multiple_of_three = random.randint(1, 50) * 3
print('Task 10:', multiple_of_three)


# 11. Получить квадратный корень из 100 и возвести в 4 степень
task_11_result = sqrt(100)**4
print('Task 11:', task_11_result)


# 12. Строку “Hi guys” вывести 3 раза и в конце добавить “Today”
# “Hi guysHi guysHi guysToday”
string_hi = "Hi guys" * 3 + "Today"
print('Task 12:', string_hi)


# 13. Получить длину строки из предыдущего задания
print('Task 13:', len(string_hi))


# 14. Взять предыдущую строку и вывести слово “Today” в прямом и обратном
# порядке)
str_today = string_hi[21:]
print('Task 14 (в прямом):', str_today)
print('Task 14 (в обратном):', str_today[::-1])


# 15. “Hi guysHi guysHi guysToday” вывести каждую 2-ю букву в прямом и обратном
# порядке
print('Task 15 (в прямом):', string_hi.replace(' ', '')[::2])
print('Task 15 (в обратном):', string_hi.replace(' ', '')[::-2])


# 16. Используя форматирования подставить результаты из задания 10 и 11 в
# следующую строку
# “Task 10: <в прямом>, <в обратном> Task 11: <в прямом>, <в обратном>”
# результат в обратном и прямом порядке
print(f'Task 16 (для Task 10): {multiple_of_three}, '
      f'{str(multiple_of_three)[::-1]}')
print(f'Task 16 (для Task 11): {task_11_result}, '
      f'{str(task_11_result)[::-1]}')


# 17. Есть строка: “my name is name”. Напечатайте ее, но вместо 2ого “name”
# вставьте ваше имя.
str_name = "my name is name"
int_first_name = str_name.find('name')
str_first = str_name[:int_first_name + 1]
str_changed = str_name[str_name.find('name') + 1:].replace("name", "Aleksey")
new_str = str_first + str_changed

print('Task 17:', new_str)


# 18. Полученную строку в задании 12 вывести:
# а) Каждое слово с большой буквы
# б) все слова в нижнем регистре
# в) все слова в верхнем регистре
print('Task 18.а : ', string_hi.title())
print('Task 18.б : ', string_hi.lower())
print('Task 18.в : ', string_hi.upper())


# 19. Посчитать сколько раз слово “Task” встречается в строке из задания 12
print('Task 19:', string_hi.count("Task"))
