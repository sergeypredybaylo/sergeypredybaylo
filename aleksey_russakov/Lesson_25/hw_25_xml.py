import xml.etree.ElementTree as ET

"""
Работа с xml файлом
Разработайте поиск книги в библиотеке по ее автору(часть имени)/цене/заголовку/
описанию.
"""

root = ET.parse('library.xml').getroot()


def find_book(param: str) -> str:
    """
    :param param: str contains author (part of name )/price/header/
    the description
    :return: the id of the book
    """
    for child in root:
        if param in child.find('author').text or\
                param in child.find('title').text or\
                param == child.find('genre').text or\
                param == child.find('price').text or\
                param in child.find('description').text:
            # print("title:", child.find('title').text)
            return child.attrib["id"]


if __name__ == "__main__":
    print(find_book("Gambardella"))
