import random
# Общий модуль
# Создать модуль, который будет предоставлять возможность выбрать одну из
# двух предыдущих программ и запустить ее, также он должен позволить по
# окончанию выполнения программы выбрать запуск другой!
# Если пользователь совершит ошибку при выборе программы
# Первые три раза программа должна сообщить ему об ошибке, а на четвертый раз
# запустить одну из программ, выбранных случайно.


def module_call(x: int):
    """
    The function imports the module that user chooses.
    """
    if x == 1:
        from bank_deposit import bank_interaction
        money = bank_interaction()
        print(round(money, 2))

    else:
        from caesar_cipher import interaction_cipher
        my_str = interaction_cipher()
        print(my_str)


def choice():
    """
    This function allows to choose what module do you want to launch:
    bank_deposit() or caesar_cipher(). The function has wrong data input
    defence.
    """
    k = 0
    user_choice = 1
    while k != 3:
        try:
            print("Какой модуль запустить?:", "1 - bank_deposit",
                  "2 - caesar_cipher", sep="\n")
            user_choice = int(input())
            if user_choice not in [1, 2]:
                k += 1
                continue
            else:
                break
        except ValueError:
            k += 1
            if k != 3:
                print("Неверный ввод")
    if k == 3:
        user_choice = random.randint(1, 2)
    module_call(user_choice)

    while True:
        try:
            print("Желаете запустить другой модуль? ", "1 - Да",
                  "2 - Нет", sep="\n")
            user_choice_end = int(input())
            if user_choice_end not in [1, 2]:
                print("Неверный ввод")
                continue
            else:
                break
        except ValueError:
            print("Неверный ввод")
    if user_choice_end == 2:
        print("Всего хорошего!")
    else:
        if user_choice == 1:
            user_choice = 2
        else:
            user_choice = 1
        module_call(user_choice)


if __name__ == "__main__":
    choice()
