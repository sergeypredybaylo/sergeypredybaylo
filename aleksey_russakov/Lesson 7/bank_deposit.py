# Написать функцию, которая вычисляет банковский вклад
# Пользователь делает вклад в размере a рублей сроком на years лет
# под 10% годовых (каждый год размер его вклада увеличивается на 10%.
# Эти деньги прибавляются к сумме вклада, и на них в следующем году
# тоже будут проценты).
# Написать функцию bank, принимающая аргументы a и years, и возвращающую
# сумму, которая будет на счету пользователя.


def bank(a: float, years: int) -> float:
    """function that calculates the bank deposit"""

    money = a
    for i in range(years):
        money += money * 0.10

    return money


def bank_interaction():
    """
    function that interacts with bank()
    """
    deposit = float(input("Введите сумму вклада: "))
    years = int(input("На сколько лет: "))
    result = bank(deposit, years)
    return result
