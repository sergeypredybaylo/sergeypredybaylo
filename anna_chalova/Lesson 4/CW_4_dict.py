import random
from itertools import zip_longest
from pip._vendor.msgpack.fallback import xrange

# 1. Создайте словарь, связав его с переменной school,
# и наполните его данными, которые бы отражали количество учащихся
# в десяти разных классах (например, 1а, 1б, 2б, 6а, 7в и т.д.).

# Задаем возможные варианты лет обучения 1-10
degree = [str(i) for i in list(xrange(1, 11))]

# Задаем возможные варианты букв классов
class_list = ['a', 'б', 'в', 'г']

# Генерируем список всех классов
all_classes = [x + y for x, y
               in zip_longest(degree, class_list,
                              fillvalue=random.choice(class_list))]

# Создаем словарь в котором количесво учеников генерируется случайно
school = {i: random.randint(20, 30) for i in all_classes}

# 2. Узнайте сколько человек в каком-нибудь классе.
rand_class = random.randint(0, 9)
print('В', all_classes[rand_class], 'классе',
      school[(all_classes[rand_class])], 'учеников.')

# 3. Представьте, что в школе произошли изменения, внесите их в словарь:
# в трех классах изменилось количество учащихся;
# в школе появилось два новых класса;
# в школе расформировали один из классов.
school[(all_classes[0])] = 1000
school[(all_classes[1])] = 1000
school[(all_classes[2])] = 1000
school['11а'] = 200
del school[(all_classes[3])]

# 4. Выведите содержимое словаря на экран.
print(school)
