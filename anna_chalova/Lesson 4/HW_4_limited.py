# 1. Перевести строку в массив
# "Robin Singh" => ["Robin”, “Singh"]
# "I love arrays they are my favorite" => ["I", "love", "arrays",
# "they", "are", "my", "favorite"]
print("Robin Singh".split(), "I love arrays they are my favorite".split(),
      sep='\n')

# 2. Дан список: [‘Ivan’, ‘Ivanou’], и 2 строки: Minsk, Belarus
# Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”
name_list = ['Ivan', 'Ivanou']
str1 = 'Minsk'
str2 = 'Belarus'
print('Привет,', name_list[0], name_list[1],
      '! Добро пожаловать в ', str1, str2)

# 3. Дан список ["I", "love", "arrays", "they", "are", "my", "favorite"]
# сделайте из него строку => "I love arrays they are my favorite"
love_list = ["I", "love", "arrays", "they", "are", "my", "favorite"]
print(' '.join(love_list))

# 4. Создайте список из 10 элементов, вставьте на 3-ю позицию
# новое значение, удалите элемент из списка под индексом 6
ten_list = [i for i in range(10)]
print(ten_list)
ten_list[2] = 11111
del ten_list[6]
print(ten_list)

# 5. Есть 2 словаря
a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': ''}

# Создать словарь, содержащий все элементы обоих словарей.
c = {**a, **b}
print(c)

# Обновите словарь “a” элементами из словаря “b”.
a.update(b)
print(a)

# Проверить, что все значения в словаре "а" не пустые либо не равны нулю.
print(0 not in a.values() and '' not in a.values())
print(all(a.values()))

# Проверить, что есть хотя бы одно пустое значение.
# Вариант 1
print('' in a.values())
# Вариант 2
print(len([i for i in a.values() if i == 0 or i == '']) > 0)
# Вариант 3
if all(a.values()) is False:
    print(True)
else:
    pass

# Отсортировать словарь по алфавиту в обратном порядке.
print(dict(sorted(a.items(), reverse=True)))

# Изменить значение под одним из ключей и вывести все значения.
a['c'] = 666
print(a)

# 6. Создать список из элементов
list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]

# Вывести только уникальные значения и сохранить в отдельную переменную.
b = [list_a[i] for i in range(len(list_a)) if list_a[i] not in list_a[0:i]]
print(b)

# Добавить в полученный объект значение '22'.
b.append(22)
print(b)

# Сделать list_a неизменяемым.
# Измерить его длину.
list_a = tuple(list_a)
print(len(list_a))
