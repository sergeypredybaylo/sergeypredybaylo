import pytest
import datetime


@pytest.fixture(autouse=True, scope='module')
def setup_module():
    start_time = datetime.datetime.now()
    print(datetime.datetime.now())
    yield start_time
    print(datetime.datetime.now())


@pytest.fixture(autouse=True, scope='module')
def print_file_name(request):
    print(request.node.name)


@pytest.fixture(autouse=True)
def name_and_time_of_called_fixture_for_each_function(setup_module, request):
    start_time = datetime.datetime.now()
    diff = start_time - setup_module
    print(diff)
    print(request.fixturenames)
