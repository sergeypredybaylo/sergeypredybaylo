def convert_to_float(number):
    if isinstance(number, int):
        return float(number)
    raise InputTypeError


class InputTypeError(Exception):
    def __init__(self, message='type must be int'):
        super().__init__(message)
