import collections
from string import punctuation

# Task 1 Перевести строку в массив
# "Robin Singh" => ["Robin”, “Singh"]
# "I love arrays they are my favorite" => ["I", "love", "arrays",
# "they", "are", "my", "favorite"]

str_name = 'Robin Singh'
str_sentence = 'I love arrays they are my favorite'

list_name = list(str_name.split(' '))
list_sentence = list(str_sentence.split(' '))
print(list_name)
print(list_sentence)


# Task 2 Дан список: [‘Ivan’, ‘Ivanou’], и 2 строки: Minsk, Belarus
# Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”

name = ['Ivan', 'Ivanou']
str_city = 'Minsk'
str_country = 'Belarus'

print(f'Привет, {name[0]} {name[1]}! Добро пожаловать в {str_city}'
      f' {str_country}')


# Task 3 Дан список ["I", "love", "arrays", "they", "are", "my", "favorite"]
# сделайте из него строку => "I love arrays they are my favorite"
list_sentence_1 = ['I', 'love', 'arrays', 'they', 'are', 'my', 'favorite']
str_sentence_1 = ' '.join(list_sentence_1)


print(str_sentence_1)


# Task 4 Создайте список из 10 элементов, вставьте на 3-ю позицию новое
# значение, удалите элемент из списка под индексом 6

list_for_task_4 = [i for i in range(10)]
list_for_task_4.insert(2, 'inserted value')


del list_for_task_4[6]
print(list_for_task_4)


# Task 5 Есть 2 словаря
#                  a = { 'a': 1, 'b': 2, 'c': 3}
#                  b = { 'c': 3, 'd': 4,'e': “”}
# 1. Создайте словарь, который будет содержать в себе все элементы обоих
# словарей
a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': ''}


joined_dict = {**a, **b}
print('joined_dict consists of ', joined_dict)

# 2. Обновите словарь “a” элементами из словаря “b”

a = {**b}
print('dict a consists of ', a)

# 3. Проверить что все значения в словаре “a” не пустые либо не равны нулю

empty_values_for_dict_a = [i for i in
                           a.values() if i]


print('empty_values_for_dict_a', all(empty_values_for_dict_a))


# 4 Проверить что есть хотя бы одно пустое значение (результат
# выполнения должен быть True)

print('' in a.values())

# 5. Отсортировать словарь по алфавиту в обратном порядке

for key, value in reversed(joined_dict.items()):
    print(key, value)


# 6 Изменить значение под одним из ключей и вывести все значения

joined_dict['d'] = 'changed value'


print(joined_dict)


# Task 6 Создать список из элементов list_a = [1,2,3,4,4,5,2,1,8]

list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]
# 1 Вывести только уникальные значения и сохранить их в отдельную переменную

unique_set = set(list_a)


print(unique_set)

# 2 Добавить в полученный объект значение 22

unique_set.add(22)

# 3 Сделать list_a неизменяемым

tuple_from_list = tuple(list_a)


print(type(tuple_from_list))

# 4 Измерить его длинну

print(len(tuple_from_list))


# Задачи на закрепление форматирования:
# Есть переменные a=10, b=25
# Вывести 'Summ is <сумма этих чисел> and diff = <их разница>.'
# При решении задачи использовать оба способа форматирования

a = 10
b = 25
summ = a + b
diff = a - b


print('Summ is {summ} and diff = {diff}.'.format(summ=summ, diff=diff))
print(f'Summ is {summ} and diff = {diff}')

# Есть список list_of_children = [“Sasha”, “Vasia”, “Nikalai”]
# Вывести “First child is <первое имя из списка>, second is “<второе>”,
# and last one – “<третье>””

list_of_children = ['Sasha', 'Vasia', 'Nikalai']
print(f'First child is {list_of_children[0]}, second is'
      f' {list_of_children[1]} and last one – {list_of_children[2]}')
print('First child is {first}, second is {second},'
      'and last one – {third}'.format(first=list_of_children[0],
                                      second=list_of_children[1],
                                      third=list_of_children[2]))


# *1) Вам передан массив чисел. Известно, что каждое число в этом массиве
# имеет пару, кроме одного: [1, 5, 2, 9, 2, 9, 1] => 5 Напишите программу,
# которая будет выводить уникальное число

non_uniq_list = [1, 5, 2, 9, 2, 9, 1]


unique_value = [i for i in non_uniq_list if non_uniq_list.count(i) == 1]
print(unique_value)


# *2) Дан текст, который содержит различные английские буквы и знаки
# препинания. Вам необходимо найти самую частую букву в тексте. Результатом
# должна быть буква в нижнем регистре. При поиске самой частой буквы,
# регистр не имеет значения, так что при подсчете считайте, что "A" == "a".

str = 'How do dddddd/you do? DSA 4324 D AfDSA D fds  ,,,,' \
      's dfsdf!!! 33342 ,.,/.,,/,/'
lower_str = str.lower()
no_digit_str = ''.join([i for i in lower_str if not i.isdigit()])
print(no_digit_str)
no_punctuation_str = ''.join(ch for ch in no_digit_str if ch not in
                             punctuation)
print('no_punctuation_str', no_punctuation_str)
no_spaces_str = no_punctuation_str.replace(' ', '')
print(no_spaces_str)
print(collections.Counter(no_spaces_str).most_common(2))
