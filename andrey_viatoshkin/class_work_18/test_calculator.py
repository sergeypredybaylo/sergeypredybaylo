import unittest

from tms.vitalii_bilyk.lecture_18.calculator import \
    find_sum, find_div, find_diff, find_multiple


class TestCalculatorFunc(unittest.TestCase):
    def test_sum_ok_1(self):
        self.assertEqual(find_sum(2, 2), 4)

    def test_sum_ok_2(self):
        self.assertEqual(find_sum(0, -2), -2)

    @unittest.expectedFailure
    def test_sum_fail_1(self):
        self.assertNotEqual(find_sum(0, -2), -2)

    def test_diff_ok_1(self):
        self.assertEqual(find_diff(2, 2), 0)

    def test_diff_ok_2(self):
        self.assertEqual(find_diff(0, -2), 2)

    @unittest.expectedFailure
    def test_diff_fail_1(self):
        self.assertNotEqual(find_diff(0, -2), 2)

    def test_multiple_ok_1(self):
        self.assertEqual(find_multiple(2, 2), 4)

    def test_multiple_ok_2(self):
        self.assertEqual(find_multiple(0, -2), 0)

    @unittest.expectedFailure
    def test_multiple_fail_1(self):
        self.assertNotEqual(find_multiple(0, -2), 0)

    def test_div_ok_1(self):
        self.assertEqual(find_div(2, 2), 1)

    def test_div_ok_2(self):
        self.assertEqual(find_div(0, -2), 0)

    @unittest.expectedFailure
    def test_div_fail_1(self):
        self.assertNotEqual(find_div(0, -2), 0)
