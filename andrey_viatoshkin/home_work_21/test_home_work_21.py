from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

# Задание 1
# На главной странице перейти по ссылке Checkboxes
# Установить галочку для второго чекбокса и проверить что она установлена
# Для первого чекбокса убрать галочку и протестировать, что ее нет
CHECKBOXES = "//*[text()='Checkboxes']"
SECOND_CHECKBOX = "//form[@id='checkboxes']/input[2]"
FIRST_CHECKBOX = "//form[@id='checkboxes']/input[1]"
MULTIPLE_WINDOWS = "//*[text()='Multiple Windows']"
CLICK_HERE = "//*[text()='Click Here']"
NEW_WINDOW_TEXT = "//h3[text()='New Window']"
ADD_REMOVE_ELEMENTS = "//*[text()='Add/Remove Elements']"
ADD_ELEMENT_BUTTON = "//*[text()='Add Element']"
NEW_ELEMENT = "//*[text()='Delete']"


def test_second_checkbox_set_to_true(driver):
    driver.find_element(By.XPATH, CHECKBOXES).click()
    driver.find_element(By.XPATH, SECOND_CHECKBOX).click()
    driver.find_element(By.XPATH, SECOND_CHECKBOX).click()
    result = driver.find_element(By.XPATH, SECOND_CHECKBOX).is_selected()
    assert result is True


def test_first_checkbox_set_to_false(driver):
    driver.find_element(By.XPATH, CHECKBOXES).click()
    driver.find_element(By.XPATH, FIRST_CHECKBOX).click()
    driver.find_element(By.XPATH, FIRST_CHECKBOX).click()
    result = driver.find_element(By.XPATH, FIRST_CHECKBOX).is_selected()
    assert result is False


# Задание 2
# На главной странице перейти по ссылке Multiple Windows
# После этого найти и нажать на ссылку Click here
# Убедиться, что открылось новое окно и проверить что заголовок страницы –
# New window

def test_new_tab(driver):
    driver.find_element(By.XPATH, MULTIPLE_WINDOWS).click()
    driver.find_element(By.XPATH, CLICK_HERE).click()
    new_tab = driver.window_handles[1]
    driver.switch_to.window(new_tab)
    new_tab_text = driver.find_element(By.XPATH, NEW_WINDOW_TEXT).text
    assert new_tab_text == 'New Window'
    tab_title = driver.title
    assert tab_title == 'New Window'


# Задание 3
# На главной странице перейти по ссылке Add/Remove elements
# Проверить что при нажатии на кнопку Add element, появляется новый элемент
# Написать отдельный тест, который проверяет удаление элементов
def test_element_add(driver):
    driver.find_element(By.XPATH, ADD_REMOVE_ELEMENTS).click()
    driver.find_element(By.XPATH, ADD_ELEMENT_BUTTON).click()
    result = driver.find_element(By.XPATH, NEW_ELEMENT).is_displayed()
    assert result is True


def test_delete_element(driver):
    driver.find_element(By.XPATH, ADD_REMOVE_ELEMENTS).click()
    driver.find_element(By.XPATH, ADD_ELEMENT_BUTTON).click()
    try:
        driver.find_element(By.XPATH, NEW_ELEMENT).click()
    except NoSuchElementException:
        pass
