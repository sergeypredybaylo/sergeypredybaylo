from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def test_radio_button():
    url = 'https://demoqa.com/radio-button'
    radio_impressive_path = '//*[@for="impressiveRadio"]'
    success_text_path = "//*[text()='Impressive']"
    driver = webdriver.Chrome()
    driver.get(url)
    waiter = WebDriverWait(driver, 10)
    waiter.until(EC.presence_of_element_located
                 ((By.XPATH, radio_impressive_path))).click()
    success_text = driver.find_element(By.XPATH, success_text_path).text
    assert 'Impressive' in success_text, "wrong button clicked"


def test_iframe():
    url = 'https://jsbin.com/cicenovile/1/edit?html,output'
    iframe_path_1 = '//iframe[@class="stretch"]'
    iframe_path_2 = '//iframe[@name="JS Bin Output "]'
    click_me_button = "//a[text()='Click me!']"
    login_button = '//*[@id="loginbtn"]'
    driver = webdriver.Chrome()
    waiter = WebDriverWait(driver, 10)
    driver.get(url)
    driver.switch_to.frame(driver.find_element(By.XPATH, iframe_path_1))
    waiter.until(EC.presence_of_element_located
                 ((By.XPATH, iframe_path_2)))
    driver.switch_to.frame(driver.find_element(By.XPATH, iframe_path_2))
    waiter.until(EC.visibility_of_element_located
                 ((By.XPATH, click_me_button))).click()
    alert = driver.switch_to.alert
    alert.accept()
    driver.switch_to.default_content()
    driver.switch_to.default_content()
    login_text = driver.find_element(By.XPATH, login_button).text
    assert 'Login' in login_text, 'wrong location'
