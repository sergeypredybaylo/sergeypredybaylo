from andrey_viatoshkin.home_work_24.pages.books_page_locators import\
    BooksPagesLocators


def test_header_name(driver, return_book_page):
    assert 'Books' == return_book_page.get_header(
        BooksPagesLocators.HEADER_SELECTOR)


def test_page_url(driver, return_book_page):
    assert 'category/books_2/' in return_book_page.get_page_url(), 'Wrong URL'


def test_tab_name(driver, return_book_page):
    assert 'Books | Oscar - Sandbox' == return_book_page.get_title(),\
        'Wrong title'
