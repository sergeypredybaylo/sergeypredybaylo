from andrey_viatoshkin.home_work_24.pages.basket_page_locators import\
    BasketPageLocators


def test_empty_basket(driver, return_basket_page):
    assert 'Your basket is empty. Continue shopping'\
           == return_basket_page.get_empty_message(), 'Basket should be empty'


def test_basket_page_header(driver, return_basket_page):
    assert 'Basket' == return_basket_page.get_header(
        BasketPageLocators.BASKET_HEADER), 'Header should be "Basket"'
