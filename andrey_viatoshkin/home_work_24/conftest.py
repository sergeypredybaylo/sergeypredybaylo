import pytest
from selenium import webdriver
from andrey_viatoshkin.home_work_24.pages.main_page import MainPage
from andrey_viatoshkin.home_work_24.pages.main_page_locators import\
    MainPageLocators
from andrey_viatoshkin.home_work_24.pages.books_page import BooksPage
from andrey_viatoshkin.home_work_24.pages.basket_page import BasketPage


@pytest.fixture(scope="session")
def driver():
    driver = webdriver.Chrome()
    yield driver
    driver.quit()


@pytest.fixture()
def return_book_page(driver):
    main_page = MainPage(driver)
    main_page.open_site()
    book_page = main_page.open_page(
        MainPageLocators.BOOKS_LINK_SELECTOR, BooksPage)
    return book_page


@pytest.fixture()
def return_basket_page(driver):
    main_page = MainPage(driver)
    main_page.open_site()
    basket_page = main_page.open_page(
        MainPageLocators.VIEW_BASKET_SELECTOR, BasketPage)
    return basket_page
