from selenium.webdriver.common.by import By


class LoginOrRegisterPageLocators:
    LOGIN_FORM = (By.ID, 'login_form')
    REGISTER_FORM = (By.ID, 'register_form')


class LoginFormLocators:
    EMAIL_FIELD = (By.ID, 'id_login-username')
    PASSWORD_FIELD = (By.ID, 'id_login-password')
    LOG_IN_BUTTON = (By.XPATH, "//button[text()='Log In']")
