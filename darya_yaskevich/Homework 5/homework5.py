# 1. Быки и коровы
# генерируем 4-значное число с неповторяющимися цифрами (str)
import string
from random import sample
number = ''.join(sample(string.digits, 4))

print('Play the "Bulls and Cows" game!')

while True:
    guess = input('Your guess: ')
    count_cow = 0
    count_bull = 0
    if guess == number:
        print('You won!')
        break
    else:
        for num in guess:
            if num in number:
                if guess.index(num) == number.index(num):
                    count_bull += 1
                else:
                    count_cow += 1
        print(f'{count_cow} cow(s), {count_bull} bull(s).')
        continue

# 2. Like
# Создайте программу, которая, принимая массив имён,
# возвращает строку описывающую количество лайков (как в Facebook).
# Введите имена через запятую: "Ann"
# -> "Ann likes this"
names = input('Введите имена через запятую: ').split(', ')

if len(names) == 1 and names[0] != '':
    print(f'{names[0]} likes this')
elif len(names) == 2:
    print(f'{names[0]} and {names[1]} like this')
elif len(names) == 3:
    print(f'{names[0]}, {names[1]} and {names[2]} like this')
elif len(names) > 3:
    print(f'{names[0]}, {names[1]} and {len(names) - 2} others like this')
else:
    print('No one likes this')

# 3. BuzzFuzz
# Напишите программу, которая перебирает последовательность от 1 до 100.
# Для чисел кратных 3 она должна написать: "Fuzz" вместо печати числа,
# а для чисел кратных 5  печатать "Buzz".
# Для чисел которые кратны 3 и 5 надо печатать "FuzzBuzz".
# Иначе печатать число.
for number in range(1, 101):
    if number % 3 == 0 and number % 5 == 0:
        print('FuzzBuzz')
    elif number % 3 == 0:
        print('Fuzz')
    elif number % 5 == 0:
        print('Buzz')
    else:
        print(number)

# 4.
# Напишите код, который возьмет список строк и пронумерует их.
# Нумерация начинается с 1, имеет “:” и пробел.
# ["a", "b", "c"] => ["1: a", "2: b", "3: c"]
my_list = ["a", "b", "c"]
list_modified = []
count = 1
if not my_list:
    print(my_list)
else:
    for element in my_list:
        element = str(count) + ':' + element
        list_modified.append(element)
        count += 1
print(list_modified)

# 5. Проверить, все ли элементы списка одинаковые.
# [1, 1, 1] == True
# [1, 2, 1] == False
# ['a', 'a', 'a'] == True
# [] == True


def is_equal(lst: list):
    if not lst:
        return True
    else:
        return len(lst) == lst.count(lst[0])


my_list = []
print(is_equal(my_list))

# 6. Проверка строки. В данной подстроке проверить все ли буквы
# в строчном регистре или нет и вернуть список не подходящих.
# doGCat => [False, ['G', 'C']]
s = 'doGCat'
s_uppercase = []

for letter in s:
    if letter.isupper():
        s_uppercase.append(letter)

flag = s.islower()
s_wrong = [flag, s_uppercase]

print(s_wrong)

# 7. Сложите все числа в списке, они могут быть отрицательными,
# если список пустой вернуть 0.
# [4, 5, 6] == 15
# range(101) == 5050
s = list(range(101))

if not s:
    print(0)
else:
    print(sum(s))
