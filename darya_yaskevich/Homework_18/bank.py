"""
This module includes the function for calculating the bank deposit.
"""


def bank(a: float, years: int):
    """This function calculates the bank deposit (a - the deposit amount).
    It returns the amount that will be on the user's account."""
    for i in range(years):
        a = a + (a * 0.1)
    return a


if __name__ == "__main__":
    print('Calculate bank deposit')
    deposit = float(input('Deposit amount: '))
    period = int(input('Investment term (years): '))
    print('Calculated account balance:', bank(deposit, period))
