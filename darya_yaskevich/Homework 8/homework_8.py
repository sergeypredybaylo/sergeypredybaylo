from random import randint
from decorators import wrap_with_words, add_one, uppercase,\
    func_name, timer, change, typed


@wrap_with_words
def random_number():
    """This function returns a random number in the range from 1 to 100."""
    number = randint(1, 100)
    return f'Random number is {number}'


@add_one
def get_number(x):
    """This function returns the number from the argument."""
    return x


@uppercase
def my_text():
    """This function returns some text in lowercase."""
    return 'some text'


@change
@func_name
def difference(a, b):
    """This function calculates the difference of the arguments."""
    return a - b


@func_name
@timer
def fib(n: int):
    f1, f2 = 0, 1
    for i in range(n):
        print(f1)
        f1, f2 = f2, f1 + f2


@wrap_with_words
@add_one
def math_expression(x: float, w: float, z: float):
    """
    This function calculates the value of the mathematical expression.
    """
    y = (5 * (3 * x ** 2 + 5 * x + 2) / (7 * w - 1 / z) - z) /\
        (4 * ((3 + x) / 7))
    return y


@typed(type_args='int')
def add_symbols(*args):
    """This function performs either concatenation for string arguments or
    addition for numbers"""
    s = ''
    if isinstance(args[0], str):
        for char in args:
            s += char
        return s
    else:
        return sum(args)


def run():
    """This function calls the selected function."""
    print('Select the function to call:',
          '1 - decorator that prints "Before" and "After" before and after'
          ' the function (function returning a random number);',
          '2 - decorator that adds 1 to the number ("get_number" function);',
          '3 - decorator that converts the given text to uppercase;',
          '4 - decorator, which makes the decorated function (calculates the'
          '  difference between two arguments) accept its unnamed arguments'
          '  in the reverse order + func_name decorator;',
          '5 - fib() function with timer and func_name decorators;',
          '6 - mathematical expression function with decorators from 1, 2;',
          '7 - decorator, that converts parameters, if necessary, and adds'
          '  them up (add_symbols function).', sep='\n')
    while True:
        choice = input('Enter the number of the function to call: ')
        while choice not in '1234567':
            choice = input('Wrong input, try again: ')
        else:
            break
    tasks = {
        "1": (random_number, [], {}),
        "2": (get_number, [6], {}),
        "3": (my_text, [], {}),
        "4": (difference, [5, 1], {}),
        "5": (fib, [5], {}),
        "6": (math_expression, [100, 12, 10], {}),
        "7": (add_symbols, [0.1, 0.2, 0.4], {}),
    }
    for key in tasks.keys():
        if key == choice:
            func, args, kwargs = tasks[key]
            print(func(*args, **kwargs))
            break


while True:
    run()
    flag = False
    again = input('Do you want to execute another function? (y/n): ')
    while again not in "yn":
        again = input('Enter "y" to choose the function, or "n" to exit: ')
    if again == "n":
        exit()
