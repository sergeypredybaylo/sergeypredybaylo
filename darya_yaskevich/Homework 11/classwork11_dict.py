s = {"a": 10, "b": 20, "c": 0, "d": -1}
# отсортировать словарь с помощью lambda функции

tuples = [i for i in sorted(s.items(), key=lambda tpl: tpl[1])]
sorted_dict = dict((t[0], t[1]) for t in tuples)
print(sorted_dict)
