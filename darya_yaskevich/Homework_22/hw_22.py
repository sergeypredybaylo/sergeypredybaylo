from selenium import webdriver
from selenium.webdriver.common.by import By
from decorator_22 import logging_decorator

DRIVER = webdriver.Chrome('C:\\data\\chromedriver_win32\\chromedriver.exe')

print("Opening main page...")
DRIVER.get('https://www.saucedemo.com/')

username_field = DRIVER.find_element(
    By.CSS_SELECTOR, 'input[placeholder="Username"]')
password_field = DRIVER.find_element(
    By.CSS_SELECTOR, 'input[placeholder="Password"]')
login_button = DRIVER.find_element(
    By.CSS_SELECTOR, 'input[class*="submit"]')

print("Log in to the website...")
username_field.send_keys('standard_user')
password_field.send_keys('secret_sauce')
login_button.submit()
print("Done. Logged in as 'standard_user'.")

# Вывести информацию по всем товарам (название, цена)
ITEMS_ON_PAGE_CSS = '.inventory_item'
ITEM_TITLE_CSS = '.inventory_item_name'
ITEM_PRICE_CSS = '.inventory_item_price'

items_list = DRIVER.find_elements(By.CSS_SELECTOR, ITEMS_ON_PAGE_CSS)
print(len(items_list), "products found:")

for item in items_list:
    product_title = item.find_element(By.CSS_SELECTOR, ITEM_TITLE_CSS).text
    product_price = item.find_element(By.CSS_SELECTOR, ITEM_PRICE_CSS).text

    print("Product title: {}\nPrice: {}".format(product_title, product_price))

# Для остальных элементов создать методы, которые находят их на странице
# и возвращают элемент.


@logging_decorator
def other_product_related_elements():
    backpack_image = DRIVER.find_element(
        By.CSS_SELECTOR, '#item_4_img_link > img')
    backpack_add_to_cart_btn = DRIVER.find_element(
        By.CSS_SELECTOR, 'button[id*="backpack"]')
    backpack_product_details_text = DRIVER.find_element(
        By.CSS_SELECTOR, '#item_4_title_link + div')
    return [
        backpack_image, backpack_add_to_cart_btn,
        backpack_product_details_text]


@logging_decorator
def footer_links():
    twitter_link = DRIVER.find_element(By.CSS_SELECTOR, '.social_twitter > a')
    facebook_link = DRIVER.find_element(By.CSS_SELECTOR,
                                        '.social_facebook > a')
    linkedin_link = DRIVER.find_element(By.CSS_SELECTOR,
                                        '.social_linkedin > a')
    return twitter_link, facebook_link, linkedin_link


@logging_decorator
def burger_menu_components():
    react_burger_menu_button = DRIVER.find_element(By.CSS_SELECTOR,
                                                   '#react-burger-menu-btn')
    all_items = DRIVER.find_element(By.CSS_SELECTOR, '#inventory_sidebar_link')
    about = DRIVER.find_element(By.CSS_SELECTOR, '#about_sidebar_link')
    logout = DRIVER.find_element(By.CSS_SELECTOR, '#logout_sidebar_link')
    reset_app_state = DRIVER.find_element(By.CSS_SELECTOR,
                                          '#reset_sidebar_link')
    cross_button = DRIVER.find_element(By.CSS_SELECTOR,
                                       '#react-burger-cross-btn')
    return [
        react_burger_menu_button, all_items, about, logout, reset_app_state,
        cross_button]


@logging_decorator
def cart_icon():
    cart_icon_element = DRIVER.find_element(
        By.CSS_SELECTOR, '.shopping_cart_link')
    return cart_icon_element


@logging_decorator
def logo():
    logo_element = DRIVER.find_element(By.CSS_SELECTOR, '.app_logo')
    return logo_element


@logging_decorator
def sorting_element():
    sorting_container = DRIVER.find_element(
        By.CSS_SELECTOR, '.product_sort_container')
    return sorting_container


if __name__ == "__main__":
    other_product_related_elements()
    footer_links()
    burger_menu_components()
    cart_icon()
    logo()
    sorting_element()
    DRIVER.quit()
