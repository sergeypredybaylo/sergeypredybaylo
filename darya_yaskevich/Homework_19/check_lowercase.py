def lowercase(word):
    """
    Check if a word is in lowercase.
    :param word: word to check
    :return: True or False
    """
    return word == word.lower()
